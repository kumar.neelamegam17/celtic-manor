package com.psommerer.app.celtic.views.supportedmodules.prevalencecalculation;

import android.content.Context;

import androidx.work.Data;

import com.psommerer.app.celtic.persistence.AppDatabaseClient;
import com.psommerer.app.celtic.persistence.MetaInfo.MetaInfoObject;
import com.psommerer.app.celtic.utils.Common;
import com.psommerer.app.celtic.views.supportedmodules.ourworldindata.Datum;
import com.psommerer.app.celtic.views.supportedmodules.ourworldindata.OurWorldInData;

import java9.util.stream.StreamSupport;

import static com.psommerer.app.celtic.utils.Common.errorLogger;
import static com.psommerer.app.celtic.utils.Common.getCurrentDateTime;

public class PrevalenceCalculation implements PrevalenceIF {

    private static final double PRE_DETECTED_DAYS = 7.5;
    private static final double ACTIVE_BLUETOOTH_RATIO = 40;
    String TAG = PrevalenceCalculation.class.getSimpleName();
    Context context;

    static double formatPercentage(double value) {
        return Common.roundFromString(String.valueOf(value * 100), 2);
    }

    @Override
    public void Prevalence(Context context) {
        this.context = context;
    }

    @Override
    public Data CalculatePrevalence(OurWorldInData ourWorldInData) {

        try {

            Datum todayDatum = StreamSupport.stream(ourWorldInData.getData()).filter(datum -> datum.getDate().equalsIgnoreCase(Common.getDate())).findAny().orElse(null);
            todayDatum = checkNull(todayDatum);

            if (todayDatum == null) {
                todayDatum = StreamSupport.stream(ourWorldInData.getData()).filter(datum -> datum.getDate().equalsIgnoreCase(Common.getDateBefore(1))).findAny().orElse(null);
                todayDatum = checkNull(todayDatum);
            }

            if (todayDatum == null) {
                todayDatum = StreamSupport.stream(ourWorldInData.getData()).filter(datum -> datum.getDate().equalsIgnoreCase(Common.getDateBefore(2))).findAny().orElse(null);
                todayDatum = checkNull(todayDatum);
            }

            if (todayDatum == null) {
                todayDatum = StreamSupport.stream(ourWorldInData.getData()).filter(datum -> datum.getDate().equalsIgnoreCase(Common.getDateBefore(4))).findAny().orElse(null);
                todayDatum = checkNull(todayDatum);
            }

            if (todayDatum == null) {
                todayDatum = StreamSupport.stream(ourWorldInData.getData()).filter(datum -> datum.getDate().equalsIgnoreCase(Common.getDateBefore(6))).findAny().orElse(null);
                todayDatum = checkNull(todayDatum);
            }

            if (todayDatum == null) {
                todayDatum = StreamSupport.stream(ourWorldInData.getData()).filter(datum -> datum.getDate().equalsIgnoreCase(Common.getDateBefore(8))).findAny().orElse(null);
                todayDatum = checkNull(todayDatum);
            }

            Datum before24Days = StreamSupport.stream(ourWorldInData.getData()).filter(datum -> datum.getDate().equalsIgnoreCase(Common.get24DateBefore())).findAny().orElse(null);

            if (todayDatum != null) {
                Double total_cumulative_cases = null;
                Double total_population = null;
                Double total_of_deaths = null;
                Double percentage_of_cases_detected = null;
                Double prevalenceRoundUp = null;
                try {
                    //Prevalence Inputs
                    Double total_number_of_COVID_deaths = Double.valueOf(todayDatum.getTotalDeaths());
                    total_cumulative_cases = Double.valueOf(todayDatum.getTotalCases());
                    Double total_cumulative_cases_as_of_24_days_ago = Double.valueOf(before24Days.getTotalCases());
                    total_population = Double.valueOf(ourWorldInData.getPopulation());

                    //Peter Sommerer calculation
                    Double Death_cases_24_days_Ago = total_number_of_COVID_deaths / total_cumulative_cases_as_of_24_days_ago;
                    Double Increase_of_cases_over_24days = total_cumulative_cases - total_cumulative_cases_as_of_24_days_ago;
                    Double expected_new_deaths = Death_cases_24_days_Ago * Increase_of_cases_over_24days;
                    total_of_deaths = total_number_of_COVID_deaths + expected_new_deaths;
                    Double observed_case_fatility_rate = formatPercentage(total_number_of_COVID_deaths / total_cumulative_cases);
                    Double extrapolated_total_cases = (total_of_deaths / observed_case_fatility_rate) * 100;
                    percentage_of_cases_detected = ((total_cumulative_cases / extrapolated_total_cases) * 100);

                    //Final calculations
                    Integer number_of_new_cases_detected = todayDatum.getNewCases();
                    Double number_of_new_cases_total = ((number_of_new_cases_detected / percentage_of_cases_detected) * 100);
                    Double number_of_predetection_infectious_people = number_of_new_cases_total * PRE_DETECTED_DAYS;
                    Double percentage_of_predetection_infectious_people = formatPercentage(number_of_predetection_infectious_people / total_population);
                    Double prevalence = (percentage_of_predetection_infectious_people / ACTIVE_BLUETOOTH_RATIO);
                    prevalenceRoundUp = Common.formatDecimal(prevalence);
                    Common.debugLogger(TAG, "Final Calculated Prevalence == " + prevalenceRoundUp);
                    PersistPrevalence(prevalenceRoundUp);

                    //insert meta information
                    Double vaccinationRate = null;
                    Double testingRate = null;
                    Double positivityRate = null;
                    if (todayDatum.getTotalVaccinations() != null) {
                        vaccinationRate = formatPercentage(todayDatum.getTotalVaccinations() / total_population);
                    } else {
                        testingRate = formatPercentage(todayDatum.getTotalTests() / total_population);
                        positivityRate = formatPercentage(total_cumulative_cases / todayDatum.getTotalTests());
                    }


                    Double infectionRate = formatPercentage(total_cumulative_cases / total_population);
                    Double fatalityRate = formatPercentage(total_of_deaths / total_cumulative_cases);
                    Double notdetected = formatPercentage((1 - (percentage_of_cases_detected / 100)));
                    PersistMetaInfo(infectionRate, testingRate, positivityRate, fatalityRate, prevalenceRoundUp, notdetected, vaccinationRate);

                    return new Data.Builder().putBoolean("status", true).build();
                } catch (Exception e) {
                    errorLogger(e, "CalculatePrevalence", context.getClass().getSimpleName());
                    AppDatabaseClient.getInstance(context).getAppDatabase().worldDataDao().updateIsActive();
                    return null;
                }
            }

        } catch (Exception e) {
            errorLogger(e, "CalculatePrevalence", context.getClass().getSimpleName());
        }
        return null;
    }

    private Datum checkNull(Datum datum) {
        if (datum != null) {
            if ((datum.getTotalTests() != null || datum.getTotalVaccinations() != null) && datum.getTotalDeaths() != null && datum.getNewCases() != null) {
                return datum;
            }
        }
        return null;
    }

    @Override
    public void PersistMetaInfo(Double infectionRate, Double testingRate, Double positivityRate, Double fatalityRate, Double prevalence, Double anonymityRate, Double vaccinationRate) {
        new Thread(() -> {
            MetaInfoObject metaInfoObject = new MetaInfoObject();
            metaInfoObject.setActive(true);
            metaInfoObject.setCreatedAt(getCurrentDateTime());
            metaInfoObject.setInfectionRate(infectionRate);
            metaInfoObject.setTestingRate(testingRate);
            metaInfoObject.setPositivityRate(positivityRate);
            metaInfoObject.setFatalityRate(fatalityRate);
            metaInfoObject.setPrevalenceRate(prevalence);
            metaInfoObject.setAnonymityRate(anonymityRate);
            metaInfoObject.setVaccinationRate(vaccinationRate);
            AppDatabaseClient.getInstance(context).getAppDatabase().metaInfoDao().insert(metaInfoObject);
        }).start();
    }

    @Override
    public void PersistPrevalence(double prevalence) {
        new Thread(() -> {
            AppDatabaseClient.getInstance(context).getAppDatabase().worldDataDao().updatePrevalenceResult(prevalence);
        }).start();
    }

}
