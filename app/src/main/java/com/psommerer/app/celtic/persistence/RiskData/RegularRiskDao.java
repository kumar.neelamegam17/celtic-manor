package com.psommerer.app.celtic.persistence.RiskData;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface RegularRiskDao {
    @Query("SELECT * FROM RegularRiskObject")
    List<RegularRiskObject> getAll();

    @Insert
    void insert(RegularRiskObject task);

    @Delete
    void delete(RegularRiskObject task);

    @Update
    void update(RegularRiskObject task);

    @Query("SELECT (CASE WHEN riskValueInt < 1 THEN 1 ELSE riskValueInt END)as risk FROM RegularRiskObject WHERE Id = (SELECT MAX(Id)  FROM RegularRiskObject);")
    int getLastRiskValue();

    @Query("DELETE FROM RegularRiskObject where createdDate<:date")
    void deleteAll(long date);


}
