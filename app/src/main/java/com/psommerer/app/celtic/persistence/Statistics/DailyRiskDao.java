package com.psommerer.app.celtic.persistence.Statistics;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Ignore;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface DailyRiskDao {
    @Query("SELECT * FROM DailyRiskObject")
    List<DailyRiskObject> getAll();

    @Insert
    void insert(DailyRiskObject task);

    @Delete
    void delete(DailyRiskObject task);

    @Update
    void update(DailyRiskObject task);

    @Ignore
    @Query("SELECT id,isActive,riskValueInt,created_at,createdTime,createdDate,latitude,longitude FROM DailyRiskObject GROUP BY date(created_at/1000, 'unixepoch')")
    List<DailyRiskObject> getDailyData();

    @Query("DELETE FROM DailyRiskObject where createdDate<:date")
    void deleteAll(long date);

    @Query("SELECT * FROM DailyRiskObject WHERE Id = (SELECT MAX(Id)  FROM DailyRiskObject where isactive=1)")
    DailyRiskObject getLastDailyRisk();


}
