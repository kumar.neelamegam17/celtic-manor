package com.psommerer.app.celtic.backgroundtask.newservice;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.JobIntentService;

import com.psommerer.app.celtic.backgroundtask.coravitaservices.CoravitaReceiver;

import org.jetbrains.annotations.NotNull;

import static com.psommerer.app.celtic.utils.Common.ALARM_SERVICE_EXECUTION_INTERVAL_IN_MS_4;

public class RestartJobIntentService extends JobIntentService {

    public static int mainJobId = 415880;
    private final Handler handler = new Handler(Looper.myLooper());
    @RequiresApi(23)
    private final Runnable restart = () -> {
        Context context = RestartJobIntentService.this.getApplicationContext();
        CoravitaReceiver.setAlarm(context, ALARM_SERVICE_EXECUTION_INTERVAL_IN_MS_4);
    };

    public static void enqueueWork(@NotNull Context context, @NotNull Intent intent) {
        JobIntentService.enqueueWork(context, RestartJobIntentService.class, mainJobId, intent);
    }

    @RequiresApi(23)
    public void onCreate() {
        super.onCreate();
        this.handler.post(this.restart);
    }

    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onHandleWork(@NonNull Intent intent) {

    }

    public boolean onStopCurrentWork() {
        return super.onStopCurrentWork();
    }


}
