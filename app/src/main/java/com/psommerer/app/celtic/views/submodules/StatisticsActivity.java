package com.psommerer.app.celtic.views.submodules;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NavUtils;

import com.psommerer.app.celtic.R;
import com.psommerer.app.celtic.databinding.ActivityStatisticsBinding;
import com.psommerer.app.celtic.models.Default14Days;
import com.psommerer.app.celtic.models.DefaultTimes;
import com.psommerer.app.celtic.persistence.AppDatabaseClient;
import com.psommerer.app.celtic.persistence.Statistics.DailyRiskObject;
import com.psommerer.app.celtic.persistence.Statistics.HourlyRiskObject;
import com.psommerer.app.celtic.utils.Common;
import com.psommerer.app.celtic.utils.MyMarkerView;
import com.psommerer.app.celtic.utils.RoundedBarChartRenderer;
import com.psommerer.app.celtic.utils.SharedPrefUtils;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.LegendEntry;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import java9.util.stream.Collectors;
import java9.util.stream.StreamSupport;

import static com.psommerer.app.celtic.utils.Common.errorLogger;
import static com.psommerer.app.celtic.utils.Common.getAllDefaultTime;
import static com.psommerer.app.celtic.utils.Common.getAppLocale;
import static com.psommerer.app.celtic.utils.Common.getEndOfDay;
import static com.psommerer.app.celtic.utils.Common.getLast14Days;
import static com.psommerer.app.celtic.utils.Common.getStartOfDay;

public class StatisticsActivity extends AppCompatActivity {

    public static final String TAG = StatisticsActivity.class.getSimpleName();
    //**********************************************************************************************
    private static final int MENU_ITEM_ITEM1 = 1;
    private static final int MENU_ITEM_ITEM2 = 2;
    ActivityStatisticsBinding activityStatisticsBinding;
    BarChart hourlyChart;
    BarChart dailyChart;
    String[] barColors = new String[]{"#009966", "#FFDE33", "#FF8821", "#CC0033", "#F5F4F2"};

    //**********************************************************************************************
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            activityStatisticsBinding = ActivityStatisticsBinding.inflate(getLayoutInflater());
            View view = activityStatisticsBinding.getRoot();
            setContentView(view);
            setUpActionBar();
            initComponent();
        } catch (Exception e) {
            errorLogger(e, "onCreate: ", TAG);
        }
    }

    private void initComponent() {
        try {
            loadHourlyChart();
            loadDailyChart();
        } catch (Exception e) {
            Common.errorLogger(e, "StatisticsActivity", TAG);
        }
    }

    public void showHourlyChart(View view) {
        activityStatisticsBinding.cardDaily.setVisibility(View.GONE);
        activityStatisticsBinding.cardHourly.setVisibility(View.VISIBLE);

        activityStatisticsBinding.btDaily.setBackgroundColor(getColor(R.color.grey_60));
        activityStatisticsBinding.btHourly.setBackgroundColor(getColor(R.color.primary));

    }

    public void showDailyChart(View view) {
        activityStatisticsBinding.cardHourly.setVisibility(View.GONE);
        activityStatisticsBinding.cardDaily.setVisibility(View.VISIBLE);

        activityStatisticsBinding.btDaily.setBackgroundColor(getColor(R.color.primary));
        activityStatisticsBinding.btHourly.setBackgroundColor(getColor(R.color.grey_60));
    }

    /**
     * Hourly risk bar chart
     */
    List<HourlyRiskObject> customHourlyBarObjects = new ArrayList<>();
    private void loadHourlyChart() throws ParseException {

        hourlyChart = activityStatisticsBinding.hourlybarchart;

        customHourlyBarObjects = new ArrayList<>();
        List<HourlyRiskObject> hourlyRiskObjects = AppDatabaseClient.getInstance(this).getAppDatabase().hourlyRiskDao().getLast24HoursData(getStartOfDay().getTime(), getEndOfDay().getTime());
        HashMap<Integer, HourlyRiskObject> hashSet = new HashMap<>();
        List<DefaultTimes> last24Hours = getAllDefaultTime(1);
        for (HourlyRiskObject object : hourlyRiskObjects) {
            for (DefaultTimes time : last24Hours) {
                Date time1 = new SimpleDateFormat(Common.FORMAT_DATETIME_STATISTICS, getAppLocale()).parse(time.getStartTime());
                Calendar calendar1 = Calendar.getInstance();
                calendar1.setTime(time1);
                calendar1.add(Calendar.DATE, 1);

                Date time2 = new SimpleDateFormat(Common.FORMAT_DATETIME_STATISTICS, getAppLocale()).parse(time.getEndTime());
                Calendar calendar2 = Calendar.getInstance();
                calendar2.setTime(time2);
                calendar2.add(Calendar.DATE, 1);

                Calendar calendar3 = Calendar.getInstance();
                calendar3.setTime(object.getCreatedAt());
                calendar3.add(Calendar.DATE, 1);

                if (calendar1.compareTo(calendar3) * calendar3.compareTo(calendar2) > 0) {
                    hashSet.put(time.getId(), object);
                }

            }
        }

        int i = 0;
        int lastKnowRisk=101;
        for (DefaultTimes defaultTimes : last24Hours) {
            Map.Entry<Integer, HourlyRiskObject> riskObject1 = StreamSupport.stream(hashSet.entrySet()).filter(datum -> datum.getKey() == defaultTimes.getId()).findAny().orElse(null);
            HourlyRiskObject customBarObject = new HourlyRiskObject();
            customBarObject.setId(i);
            if (riskObject1 != null) {
                lastKnowRisk=riskObject1.getValue().getRiskValueInt();
                customBarObject.setLatitude(riskObject1.getValue().getLatitude());
                customBarObject.setLongitude(riskObject1.getValue().getLongitude());
            }
            customBarObject.setRiskValueInt(lastKnowRisk);
            customBarObject.setActive(true);
            customHourlyBarObjects.add(customBarObject);
            i++;
        }

        renderHourlyChart(customHourlyBarObjects);


    }

    /**
     * Daily risk bar chart
     */
    List<DailyRiskObject> customDailyBarObjects = new ArrayList<>();
    private void loadDailyChart() {
        dailyChart = activityStatisticsBinding.dailybarchart;

        customDailyBarObjects = new ArrayList<>();
        List<DailyRiskObject> riskObject = AppDatabaseClient.getInstance(this).getAppDatabase().dailyRiskDao().getDailyData();
        HashMap<Integer, DailyRiskObject> hashSet = new HashMap<>();

        List<Default14Days> last14Days = getLast14Days(1);

        for (DailyRiskObject object : riskObject) {
            Date currentDate = object.getCreatedAt();
            for (Default14Days last14Day : last14Days) {
                if (currentDate.getTime() < last14Day.getNewDay().getTime()) {
                    hashSet.put(last14Day.getId(), object);
                }
            }
        }


        int i = 0;
        for (Default14Days last14Day : last14Days) {
            Map.Entry<Integer, DailyRiskObject> riskObject1 = StreamSupport.stream(hashSet.entrySet()).filter(datum -> datum.getKey() == last14Day.getId()).findAny().orElse(null);
            DailyRiskObject dailyRiskObject = new DailyRiskObject();
            dailyRiskObject.setId(i);
            if (riskObject1 != null) {
                dailyRiskObject.setRiskValueInt(riskObject1.getValue().getRiskValueInt());
                dailyRiskObject.setActive(true);
                dailyRiskObject.setLatitude(riskObject1.getValue().getLatitude());
                dailyRiskObject.setLongitude(riskObject1.getValue().getLongitude());
            } else {
                dailyRiskObject.setRiskValueInt(101);
                dailyRiskObject.setActive(false);
            }
            customDailyBarObjects.add(dailyRiskObject);
            i++;
        }

        renderDailyChart(customDailyBarObjects);

    }


    private void addChartDefaultParameters(BarChart barChart) {
        barChart.getLegend().setEnabled(true);
        barChart.getDescription().setEnabled(false);
        barChart.getAxisRight().setEnabled(true);
        barChart.getAxisLeft().setEnabled(true);
        barChart.setDoubleTapToZoomEnabled(true);
        barChart.setDrawValueAboveBar(false);
        barChart.setDrawGridBackground(false);
        barChart.getAxisLeft().setDrawGridLines(false);
        barChart.getXAxis().setDrawGridLines(false);
        barChart.getXAxis().setDrawAxisLine(false);
        barChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTH_SIDED);
        barChart.setTouchEnabled(true);
        barChart.setPinchZoom(true);
        MyMarkerView mv = new MyMarkerView(getApplicationContext(), R.layout.custom_marker_view);
        mv.setChartView(barChart);
        barChart.setMarker(mv);

        YAxis leftAxis = barChart.getAxisLeft();
        leftAxis.setDrawGridLines(true);
        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        leftAxis.setAxisMaximum(100);
        leftAxis.setAxisMinimum(0);

        YAxis rightAxis = barChart.getAxisRight();
        rightAxis.setDrawGridLines(true);
        rightAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        rightAxis.setAxisMaximum(100);
        rightAxis.setAxisMinimum(0);

    }

    private void renderHourlyChart(List<HourlyRiskObject> riskObject) {

        addChartDefaultParameters(hourlyChart);

        List<String> todayDatum = StreamSupport.stream(getAllDefaultTime(2)).map(DefaultTimes::getStartTime).collect(Collectors.toList());

        XAxis xAxis = hourlyChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.TOP);
        xAxis.setGranularity(1);
        xAxis.setValueFormatter(new IndexAxisValueFormatter(todayDatum));

        List<HourlyRiskObject> riskdata = getRiskHourlyData(riskObject);

        List<BarEntry> entryData = new ArrayList<>();
        List<Integer> entryColor = new ArrayList<>();

        int i = 0;
        for (HourlyRiskObject riskdatum : riskdata) {
            float aFloat = Float.parseFloat(String.valueOf(riskdatum.getRiskValueInt()));
            entryData.add(new BarEntry(i, aFloat));

            if (riskdatum.isActive()) {
                if (aFloat >= 0.0F && aFloat <= 20.0F) {
                    entryColor.add(Color.parseColor(this.barColors[0]));
                } else if (aFloat >= 20.0F && aFloat <= 50.0F) {
                    entryColor.add(Color.parseColor(this.barColors[1]));
                } else if (aFloat >= 50.0F && aFloat <= 90.0F) {
                    entryColor.add(Color.parseColor(this.barColors[2]));
                } else if (aFloat >= 90.0F && aFloat <= 100.0F) {
                    entryColor.add(Color.parseColor(this.barColors[3]));
                } else {
                    entryColor.add(Color.parseColor(this.barColors[4]));
                }
            } else {
                entryColor.add(Color.parseColor(this.barColors[4]));
            }

            i++;
        }


        BarDataSet barDataSet = new BarDataSet(entryData, "");
        barDataSet.setColors(entryColor);
        barDataSet.setDrawIcons(false);
        barDataSet.setDrawValues(false);

        BarData barData = new BarData(barDataSet);

        RoundedBarChartRenderer roundedBarChartRenderer = new RoundedBarChartRenderer(hourlyChart, hourlyChart.getAnimator(), hourlyChart.getViewPortHandler());
        roundedBarChartRenderer.setmRadius(15f);

        hourlyChart.setRenderer(roundedBarChartRenderer);

        hourlyChart.getLegend().setCustom(barChartLegends());

        barDataSet.setValueTextSize(12f);
        hourlyChart.setData(barData);
        hourlyChart.animateY(1500);
        hourlyChart.invalidate();
    }

    private void renderDailyChart(List<DailyRiskObject> riskObject) {

        addChartDefaultParameters(dailyChart);

        List<String> days = StreamSupport.stream(getLast14Days(2)).map(Default14Days::getDateStr).collect(Collectors.toList());
        XAxis xAxis = dailyChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.TOP);
        xAxis.setGranularity(1);
        xAxis.setValueFormatter(new IndexAxisValueFormatter(days));

        List<DailyRiskObject> riskdata = getDailyRiskData(riskObject);

        List<BarEntry> entryData = new ArrayList<>();
        List<Integer> entryColor = new ArrayList<>();

        int i = 0;
        for (DailyRiskObject riskdatum : riskdata) {
            float aFloat = Float.parseFloat(String.valueOf(riskdatum.getRiskValueInt()));
            entryData.add(new BarEntry(i, aFloat));

            if (riskdatum.isActive()) {
                if (aFloat >= 0.0F && aFloat <= 20.0F) {
                    entryColor.add(Color.parseColor(this.barColors[0]));
                } else if (aFloat >= 20.0F && aFloat <= 50.0F) {
                    entryColor.add(Color.parseColor(this.barColors[1]));
                } else if (aFloat >= 50.0F && aFloat <= 90.0F) {
                    entryColor.add(Color.parseColor(this.barColors[2]));
                } else if (aFloat >= 90.0F && aFloat <= 100.0F) {
                    entryColor.add(Color.parseColor(this.barColors[3]));
                } else {
                    entryColor.add(Color.parseColor(this.barColors[4]));
                }
            } else {
                entryColor.add(Color.parseColor(this.barColors[4]));
            }

            i++;
        }


        BarDataSet barDataSet = new BarDataSet(entryData, "");
        barDataSet.setColors(entryColor);
        barDataSet.setDrawIcons(false);
        barDataSet.setDrawValues(false);

        BarData barData = new BarData(barDataSet);

        RoundedBarChartRenderer roundedBarChartRenderer = new RoundedBarChartRenderer(dailyChart, dailyChart.getAnimator(), dailyChart.getViewPortHandler());
        roundedBarChartRenderer.setmRadius(10f);

        dailyChart.setRenderer(roundedBarChartRenderer);

        dailyChart.getLegend().setCustom(barChartLegends());

        barDataSet.setValueTextSize(12f);
        dailyChart.setData(barData);
        dailyChart.animateY(1500);
        dailyChart.invalidate();


    }


    //**********************************************************************************************

    private List<HourlyRiskObject> getRiskHourlyData(List<HourlyRiskObject> riskObject) {
        Collections.sort(riskObject, new SortByIdHourly());
        return riskObject;
    }

    private List<DailyRiskObject> getDailyRiskData(List<DailyRiskObject> riskObject) {
        Collections.sort(riskObject, new SortByIdDaily());
        return riskObject;
    }

    //**********************************************************************************************

    public List<LegendEntry> barChartLegends() {

        LegendEntry legendA = new LegendEntry();
        legendA.label = getString(R.string.low_risk_sl);
        legendA.formColor = Color.parseColor(barColors[0]);

        LegendEntry legendB = new LegendEntry();
        legendB.label = getString(R.string.moderate_risk_sl);
        legendB.formColor = Color.parseColor(barColors[1]);

        LegendEntry legendC = new LegendEntry();
        legendC.label = getString(R.string.high_risk_sl);
        legendC.formColor = Color.parseColor(barColors[2]);

        LegendEntry legendD = new LegendEntry();
        legendD.label = getString(R.string.veryhigh_risk_sl);
        legendD.formColor = Color.parseColor(barColors[3]);

        return Arrays.asList(legendA, legendB, legendC, legendD);
    }

    //**********************************************************************************************

    private void setUpActionBar() {
        setUpTheme();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle(getResources().getString(R.string.statistics_activity_title));
    }

    //**********************************************************************************************
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    //**********************************************************************************************
    public void setUpTheme() {
        SharedPrefUtils sharedPrefUtils = SharedPrefUtils.getInstance(this);
        if (sharedPrefUtils.isDarkMode()) {
            setTheme(R.style.AppTheme_Dark);
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorthemeDarkPrimary)));
        } else {
            setTheme(R.style.AppTheme_Light);
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorthemeLightPrimary)));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(1, MENU_ITEM_ITEM1, Menu.NONE, getString(R.string.refresh)).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_WITH_TEXT);
        menu.add(2, MENU_ITEM_ITEM2, Menu.NONE, getString(R.string.map)).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_WITH_TEXT);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case MENU_ITEM_ITEM1:
                recreate();
                Toast.makeText(getApplicationContext(), R.string.updated, Toast.LENGTH_LONG).show();
                return true;
            case MENU_ITEM_ITEM2:
                Intent intent=new Intent(this, MapInfoActivity.class);
                intent.putParcelableArrayListExtra("dailylist", (ArrayList<? extends Parcelable>) customDailyBarObjects);
                intent.putParcelableArrayListExtra("hourlylist", (ArrayList<? extends Parcelable>) customHourlyBarObjects);
                startActivity(intent);
                return true;
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            default:
                return false;
        }
    }
    //**********************************************************************************************

    class SortByIdHourly implements Comparator<HourlyRiskObject> {
        public int compare(HourlyRiskObject a, HourlyRiskObject b) {
            return a.getId() - b.getId();
        }
    }

    class SortByIdDaily implements Comparator<DailyRiskObject> {
        public int compare(DailyRiskObject a, DailyRiskObject b) {
            return a.getId() - b.getId();
        }
    }

    //**********************************************************************************************


}
