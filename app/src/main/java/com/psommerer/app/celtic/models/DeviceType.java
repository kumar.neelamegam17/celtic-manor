package com.psommerer.app.celtic.models;

public enum DeviceType {
    CLASSIC, LE, DUAL, UNKNOWN
}
