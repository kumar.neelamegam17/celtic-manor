package com.psommerer.app.celtic.utils;

import android.animation.ObjectAnimator;
import android.app.ActivityManager;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothClass;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.animation.BounceInterpolator;
import android.widget.Toast;

import androidx.work.Constraints;
import androidx.work.Data;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;

import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.google.gson.Gson;
import com.psommerer.app.celtic.R;
import com.psommerer.app.celtic.backgroundtask.workers.InsertDataWorker;
import com.psommerer.app.celtic.backgroundtask.workers.PrevalenceDataWorker;
import com.psommerer.app.celtic.backgroundtask.workers.RiskCalculationWorker;
import com.psommerer.app.celtic.models.Default14Days;
import com.psommerer.app.celtic.models.DefaultTimes;
import com.psommerer.app.celtic.models.ScanBluetooth;
import com.psommerer.app.celtic.persistence.AppDatabaseClient;
import com.psommerer.app.celtic.persistence.SettingsData.AppSettingsObject;
import com.psommerer.app.celtic.persistence.WorldData.WorldData;
import com.psommerer.app.celtic.views.supportedmodules.riskcalculation.Utils;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.math.BigDecimal;
import java.math.MathContext;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Set;
import java.util.TimeZone;

import java9.util.stream.Collectors;
import java9.util.stream.StreamSupport;


public class Common {
    /* ********************************************************************************************** */
    public static final String OUR_WORLD_IN_DATA_JSON = "/data/owid-covid-data.json";
    public static String TAG = Common.class.getSimpleName();
    public static Integer LOCATION_INTERVAL = 600000;//600000ms 10 Minutes
    public static long ALARM_SERVICE_EXECUTION_INTERVAL_IN_MS_5 = 300000;//300000ms to every 5 minutes
    public static long ALARM_SERVICE_EXECUTION_INTERVAL_IN_MS_4 = 240000;//240000ms to every 4 minutes
    public static Integer NUMBER_OF_SCANNING_ITERATION = 12;//12 round
    public static boolean DAILYNOTIFICATION_FLAG = false;
    public static Integer INTERVAL_TO_STOP_SCANNING_IN_MS = 12000;//12 seconds
    public static Integer NO_OF_UNIQUE_DEVICE_COUNT = 3;
    public static String FORMAT_ONLY_TIME_WITHTIMEZONE = "HH:mm z";
    public static String FORMAT_ONLY_TIME_WITHOUTTIMEZONE = "HH:mm";
    public static String FORMAT_ONLY_DATE_WITHOUTTIMEZONE = "dd-MM-yyyy";
    public static String FORMAT_ONLY_DATE_WITH_YEAR = "dd-MM-yy";
    public static String FORMAT_DATETIME = "dd-MM-yyyy HH:mm z";
    public static String FORMAT_DATETIME_STATISTICS = "dd-MM-yyyy HH:mm";
    public static String KEY_DARKMODE = "darkmode";
    public static String KEY_HARDLOAD = "hardload";
    public static String KEY_NOTIFICATIONSTATUS = "notificationstatus";
    public static String KEY_SERVICESTATUS = "servicestatus";
    public static String KEY_NETWORKSTATUS = "networkstatus";
    public static String KEY_DISCLAIMERSTATUS = "disclaimerstatus";
    public static String KEY_LATITUDE = "latitude";
    public static String KEY_LONGITUDE = "longitude";
    public static String KEY_CURRENTCOUNTRY = "country";
    public static String KEY_CURRENTCOUNTRYCODE = "countrycode";
    public static String KEY_CURRENTCITY = "city";
    public static String KEY_LASTRISKVALUE = "riskvalue";
    public static String KEY_COUNTERTAG = "countertag";
    public static String KEY_CURRENT_COUNTERTAG = "currenttag";
    public static String KEY_SCANNED_DATA = "scanneddata";
    public static String KEY_CURRENTCOUNT_TAG = "ctag";
    public static String KEY_NOTIFICATION_TAG = "notificationId";

    public static String SIMPLEDATEFORMAT = "yyyy-MM-dd";
    public static int LOW_RISK_THRESHOLD = 1;

    public static String WORKER_TAG_INSERTDATA = "WORKER_TAG_INSERTDATA";
    public static String WORKER_TAG_PREVALENCECALCULATION = "WORKER_TAG_PREVALENCECALCULATION";
    public static String WORKER_TAG_RISKCALCULATION = "WORKER_TAG_RISKCALCULATION";
    public static String WORKER_TAG_HOURLYNOTIFICATION = "WORKER_TAG_HOURLYNOTIFICATION";
    public static String WORKER_TAG_DAILYNOTIFICATION = "WORKER_TAG_DAILYNOTIFICATION";

    public static ArrayList<ScanBluetooth> scannedDevicesList = new ArrayList<>();
    public static int DEFAULT_CONTACT_DISTANCE = 2;
    public static int DEFAULT_SCAN_INTERVAL = 15;

    /* ********************************************************************************************** */

    public static Date getCurrentDateTime() {
        return Calendar.getInstance().getTime();
    }

    public static String getDateString(Date dateTime) {
        if (dateTime != null) {
            SimpleDateFormat sdf = new SimpleDateFormat(FORMAT_DATETIME, Common.getAppLocale());
            sdf.setTimeZone(DateTimeZone.UTC.toTimeZone());
            return sdf.format(dateTime);
        }
        return "-";
    }

    public static String getTimeString(Date dateTime) {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:00", getAppLocale());
        sdf.setTimeZone(getAppDefaultTimeZone());
        return sdf.format(dateTime);
    }

    public static Date getOnlyDateWOTZ(Date date) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat(FORMAT_ONLY_DATE_WITHOUTTIMEZONE, getAppLocale());
        formatter.setTimeZone(getAppDefaultTimeZone());
        return formatter.parse(formatter.format(date));
    }

    public static Date getOnlyTimeWOTZ(Date date) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat(FORMAT_ONLY_TIME_WITHOUTTIMEZONE, getAppLocale());
        formatter.setTimeZone(getAppDefaultTimeZone());
        return formatter.parse(formatter.format(date));
    }

    public static String getDailyNotificationTimeString() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 9);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        SimpleDateFormat sdf = new SimpleDateFormat(FORMAT_ONLY_TIME_WITHTIMEZONE, getAppLocale());
        sdf.setTimeZone(getAppDefaultTimeZone());
        return sdf.format(calendar.getTime());
    }


    public static String getDate() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat formatter = new SimpleDateFormat(SIMPLEDATEFORMAT, getAppLocale());
        formatter.setTimeZone(getAppDefaultTimeZone());
        return formatter.format(calendar.getTime());
    }

    public static Date getStartOfDay() {
        DateTime dateTime = new DateTime();
        return dateTime.minusDays(1).toDate();
    }

    public static Date getEndOfDay() {
        DateTime dateTime = new DateTime();
        return dateTime.toDate();
    }

    public static String get24DateBefore() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_MONTH, -24);
        Date sevenDaysAgo = cal.getTime();
        SimpleDateFormat formatter = new SimpleDateFormat(SIMPLEDATEFORMAT, Common.getAppLocale());
        formatter.setTimeZone(getAppDefaultTimeZone());
        return formatter.format(sevenDaysAgo.getTime());
    }

    public static String getDateBefore(int days) {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_MONTH, -days);
        Date sevenDaysAgo = cal.getTime();
        SimpleDateFormat formatter = new SimpleDateFormat(SIMPLEDATEFORMAT, Common.getAppLocale());
        formatter.setTimeZone(getAppDefaultTimeZone());
        //debugLogger(TAG, formatter.format(sevenDaysAgo.getTime()));
        return formatter.format(sevenDaysAgo.getTime());
    }

    /* ********************************************************************************************** */

    public static void showToast(String message, int duration, Context context) {
        Toast.makeText(context, message, duration).show();
    }
    /* ********************************************************************************************** */

    public static void errorLogger(Exception e, String message, String TAG) {
        if (e != null) {
            Log.e(TAG, "errorLogger: " + message, e);
            FirebaseCrashlytics.getInstance().log(Objects.requireNonNull(e.getMessage()));
            //appendLog(getCurrentDateTime()+"-"+e.getMessage());
        }
    }

    public static void debugLogger(String tag, String message) {
        //Log.e(tag, message);
        //appendLog(getCurrentDateTime()+"-"+tag+"-->"+message);
    }

    /* ********************************************************************************************** */


    public static void doBounceAnimation(View targetView, int repeatCount) {
        ObjectAnimator animator = ObjectAnimator.ofFloat(targetView, "translationY", 0, 100, 0);
        animator.setInterpolator(new BounceInterpolator());
        animator.setStartDelay(500);
        animator.setDuration(2500);
        animator.setRepeatCount(repeatCount);
        animator.start();
    }
    /* ********************************************************************************************** */

    public static void setUpTheme(Context ctx) {
        SharedPrefUtils sharedPrefUtils = SharedPrefUtils.getInstance(ctx);
        if (sharedPrefUtils.isDarkMode()) ctx.setTheme(R.style.AppTheme_Dark);
        else ctx.setTheme(R.style.AppTheme_Light);
    }
    /* ********************************************************************************************** */

    public static String getDistance(int rssi) {
        /*
         * RSSI = TxPower - 10 * n * lg(d)
         * n = 2 (in free space)
         * d = 10 ^ ((TxPower - RSSI) / (10 * n))
         */
        int txPower = -59; //hard coded power value. Usually ranges between -59 to -65
        double result = Math.pow(10d, ((double) txPower - rssi) / (10 * 2));
        //DecimalFormat df = new DecimalFormat("0.00");
        return String.format(Common.getAppLocale(), "%.2f", result);
    }

    /* ********************************************************************************************** */
    public static String getCountryCodeFromCountry(String countryCode) {
        String[] isoCountries = Locale.getISOCountries();
        String countryCodeISO;
        for (String country : isoCountries) {
            Locale locale = new Locale(getAppLocale().toLanguageTag(), country);
            String iso = locale.getISO3Country();
            String code = locale.getCountry();
            if (countryCode.equalsIgnoreCase(code)) {
                // Common.debugLogger("CodeFromCountry", iso + " " + code);
                countryCodeISO = iso;
                return countryCodeISO;
            }
        }
        return null;
    }

    /* ********************************************************************************************* */
    public static int convertRiskToInt(String riskValue) {
        int valueFinal = Math.round(Float.parseFloat(riskValue) * 100);
        if (valueFinal < LOW_RISK_THRESHOLD) {
            return LOW_RISK_THRESHOLD;
        } else {
            return valueFinal;
        }
    }

    /* ********************************************************************************************** */
    public static int checkRiskScalesInt(int riskValue) {
        Common.debugLogger(TAG, "checkRiskScales Before Int: " + riskValue);
        if (riskValue >= 0 && riskValue <= 20) {
            return 1;
        } else if (riskValue >= 21 && riskValue <= 50) {
            return 2;
        } else if (riskValue >= 51 && riskValue <= 90) {
            return 3;
        } else if (riskValue >= 91) {
            return 4;
        }
        return 0;
    }

    public static String checkRiskScalesString(int riskValue, Context context) {
        Common.debugLogger(TAG, "checkRiskScalesString: " + riskValue);
        if (riskValue >= 0 && riskValue <= 20) {
            return context.getString(R.string.low_risk);
        } else if (riskValue >= 21 && riskValue <= 50) {
            return context.getString(R.string.moderate_risk);
        } else if (riskValue >= 51 && riskValue <= 90) {
            return context.getString(R.string.high_risk);
        } else if (riskValue >= 91) {
            return context.getString(R.string.veryhigh_risk);
        }
        return context.getString(R.string.low_risk);
    }

    /* ********************************************************************************************** */
    public static String checkStringEmpty(String value) {
        if (value != null && !value.isEmpty()) {
            return value;
        } else {
            return "-";
        }
    }

    /* ********************************************************************************************** */
    public static boolean isMyServiceRunning(Class<?> serviceClass, Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    /* ********************************************************************************************** */
    public static Locale getAppLocale() {
        return Locale.ENGLISH;
    }

    public static TimeZone getAppDefaultTimeZone(){
        return DateTimeZone.UTC.toTimeZone();
    }

    /* ********************************************************************************************** */

    public static double formatDecimal(double prevalence) {
        return BigDecimal.valueOf(prevalence).movePointRight(2).round(MathContext.UNLIMITED).movePointLeft(2).doubleValue();
    }


    public static double roundFromString(String num, int digits) {
        // epsilon correction
        double n = Double.longBitsToDouble(Double.doubleToLongBits(Double.parseDouble(num)) + 1);
        double p = Math.pow(10, digits);
        double result = Math.round(n * p) / p;
        debugLogger(TAG, "After roundFromString: " + result);
        return result;
    }


    /* ********************************************************************************************** */
    public static void runOurWorldData(Context context) {
        Constraints myConstraints = new Constraints.Builder().setRequiredNetworkType(NetworkType.CONNECTED).build();
        OneTimeWorkRequest runWorldDataRetrievalWorker = new OneTimeWorkRequest.Builder(PrevalenceDataWorker.class).setConstraints(myConstraints)
                .addTag(Common.WORKER_TAG_PREVALENCECALCULATION)
                .build();
        WorkManager.getInstance(context).enqueue(runWorldDataRetrievalWorker);
    }

    /* ********************************************************************************************** */
    public static String checkMetaInfoIsValid(Double v) {
        if (v != null && v != 0.0) {
            return String.format(Common.getAppLocale(), "%.2f%%", v);
        }
        return "-";
    }

    /* ********************************************************************************************** */
    public static String checkMetaInfoVaccination(Double v) {
        if (v != null && v != 0.0) {
            return String.format(Common.getAppLocale(), "%.2f%%", v);
        }
        return "0";
    }


    public static String convertPrevalenceData(double value) {

        if (value < 0.001) {
            debugLogger(TAG, "convertPrevalenceData lesser: " + value);
            return String.format(Common.getAppLocale(), "<%s%%", "0.1");
        } else {
            value = value * 100;
            value = Math.floor(value * 100) / 100;
            debugLogger(TAG, "convertPrevalenceData ok: " + value);
            return String.format(Common.getAppLocale(), "%s%%", value);
        }
    }
    /* ********************************************************************************************** */

    public static List<DefaultTimes> getAllDefaultTime(int id) {
        List<DefaultTimes> times = new ArrayList<>();
        DateTime dateTime = new DateTime();
        int i = 24;
        for (int i1 = i; i1 > 0; i1--) {
            SimpleDateFormat sdf;
            if (id == 1) {//for comparisonc
                sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm", getAppLocale());
            } else {//for display
                sdf = new SimpleDateFormat("HH:mm", getAppLocale());
            }
            times.add(new DefaultTimes(i1, sdf.format(dateTime.minusHours(i1).toDate().getTime()), sdf.format(dateTime.minusHours(i1 - 1).toDate().getTime())));
        }
        return times;
    }

    public static List<Default14Days> getLast14Days(int id) {
        DateTime dateTime = new DateTime();
        List<Default14Days> default14Days = new ArrayList<>();
        int hourHand = 14;
        for (int i1 = hourHand; i1 > 0; i1--) {
            SimpleDateFormat sdf;
            if (id == 1) {//for comparison
                sdf = new SimpleDateFormat(Common.FORMAT_ONLY_DATE_WITHOUTTIMEZONE, getAppLocale());
            } else {//for display
                sdf = new SimpleDateFormat(Common.FORMAT_ONLY_DATE_WITH_YEAR, getAppLocale());
            }

            sdf.setTimeZone(getAppDefaultTimeZone());
            default14Days.add(new Default14Days(i1, dateTime.minusDays(i1).toDate(), sdf.format(dateTime.minusDays(i1).toDate().getTime())));
        }
        return default14Days;
    }

    /* ********************************************************************************************** */

    public static void insertScanDataWorker(Context ctx, String latitude, String longitude, int currentCountTag) {
        try {
            Set<ScanBluetooth> filterList = StreamSupport.stream(scannedDevicesList).distinct().collect(Collectors.toSet());
            String jsonText = new Gson().toJson(filterList);
            Data data = new Data.Builder()
                    .putString(KEY_LATITUDE, latitude)
                    .putString(KEY_LONGITUDE, longitude)
                    .putInt(KEY_COUNTERTAG, currentCountTag)
                    .putString(KEY_SCANNED_DATA, jsonText)
                    .build();
            OneTimeWorkRequest runWorldDataRetrievalWorker = new OneTimeWorkRequest.Builder(InsertDataWorker.class).setInputData(data)
                    .addTag(Common.WORKER_TAG_INSERTDATA)
                    .build();
            WorkManager.getInstance(ctx).enqueue(runWorldDataRetrievalWorker);
            scannedDevicesList = new ArrayList<>();
            BluetoothAdapter bAdapter = BluetoothAdapter.getDefaultAdapter();
            debugLogger(TAG, "Bluetooth adapter status:"+bAdapter.getState());
            if (!bAdapter.isEnabled()) {
                bAdapter.enable();
            }
        } catch (Exception e) {
            errorLogger(e, e.getMessage(), TAG);
        }
    }

    /* ********************************************************************************************** */
    public static void addScannedDeviceToList(BluetoothDevice bluetoothDevice) {
        if (bluetoothDevice != null) {
            int deviceClass = bluetoothDevice.getBluetoothClass().getMajorDeviceClass();
            if (deviceClass == BluetoothClass.Device.Major.PHONE ||
                    deviceClass == BluetoothClass.Device.Major.AUDIO_VIDEO ||
                    deviceClass == BluetoothClass.Device.Major.WEARABLE ||
                    deviceClass == BluetoothClass.Device.Major.UNCATEGORIZED ||
                    deviceClass == BluetoothClass.Device.Major.MISC) {
                scannedDevicesList.add(new ScanBluetooth(bluetoothDevice.getAddress()));
            }
        }
    }

    /* ********************************************************************************************** */

    public static void runRiskCalculator(Context ctx, int currentCountTag) {
        Data data = new Data.Builder().putInt(KEY_CURRENTCOUNT_TAG, currentCountTag).build();
        OneTimeWorkRequest runRiskCalculationEveryHour = new OneTimeWorkRequest.Builder(RiskCalculationWorker.class)
                .addTag(Common.WORKER_TAG_RISKCALCULATION)
                .setInputData(data)
                .build();
        WorkManager.getInstance(ctx).enqueue(runRiskCalculationEveryHour);
    }

    /* ********************************************************************************************** */


    public static String getRiskValueForLast24Hours(Context ctx) {
        try {
            WorldData worldData = AppDatabaseClient.getInstance(ctx).getAppDatabase().worldDataDao().getLastWorldData();
            String currentPrevalence = worldData.getCalculatedPrevalence();

            int minimumDeviceCount = Common.NO_OF_UNIQUE_DEVICE_COUNT;
            double modifiedPrevalence = Double.parseDouble(currentPrevalence);
            AppSettingsObject appSettingsObject = AppDatabaseClient.getInstance(ctx).getAppDatabase().appSettingsDao().getSettings();

            if (appSettingsObject.getCount_interval() == 5) {
                minimumDeviceCount = 1;
                modifiedPrevalence = (Double.parseDouble(currentPrevalence) * 1) / 3;
            } else if (appSettingsObject.getCount_interval() == 10) {
                minimumDeviceCount = 2;
                modifiedPrevalence = (Double.parseDouble(currentPrevalence) * 2) / 3;
            } else if (appSettingsObject.getCount_interval() == 15) {
                minimumDeviceCount = 3;
                modifiedPrevalence = Double.parseDouble(currentPrevalence);
            }
            DateTime dateTime = new DateTime();
            int deviceCount = AppDatabaseClient.getInstance(ctx).getAppDatabase().scanDao().getDeviceCountOfLast1Day((dateTime.minusDays(1).toDate().getTime()),
                    (dateTime.minusDays(0).toDate().getTime()), minimumDeviceCount);
            int X = 1;
            BigDecimal equalResult = Utils.equal(modifiedPrevalence, deviceCount, X);
            BigDecimal greaterResult = Utils.greater(modifiedPrevalence, deviceCount, X);
            BigDecimal greaterEqualResult = equalResult.add(greaterResult);
            return Utils.getPrettyNum(greaterEqualResult);
        } catch (NumberFormatException e) {
            errorLogger(e, e.getMessage(), TAG);
        }
        return "0.0";
    }


}
