package com.psommerer.app.celtic.backgroundtask.workers;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.work.Data;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.psommerer.app.celtic.persistence.AppDatabaseClient;
import com.psommerer.app.celtic.persistence.RiskData.RiskObject;
import com.psommerer.app.celtic.persistence.WorldData.WorldData;
import com.psommerer.app.celtic.utils.Common;
import com.psommerer.app.celtic.utils.SharedPrefUtils;
import com.psommerer.app.celtic.views.supportedmodules.ourworldindata.OurWorldInData;
import com.psommerer.app.celtic.views.supportedmodules.ourworldindata.OurWorldInDataService;
import com.psommerer.app.celtic.views.supportedmodules.prevalencecalculation.PrevalenceCalculation;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;

import java.util.List;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.psommerer.app.celtic.utils.Common.errorLogger;
import static com.psommerer.app.celtic.utils.Common.getCountryCodeFromCountry;
import static com.psommerer.app.celtic.utils.Common.getCurrentDateTime;

public class PrevalenceDataWorker extends Worker {

    SharedPrefUtils sharedPrefUtils;
    Context context;
    String TAG = PrevalenceDataWorker.class.getSimpleName();
    String worldURL = "https://covid.ourworldindata.org";

    public PrevalenceDataWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
        this.sharedPrefUtils = new SharedPrefUtils(context);
        this.context = context;
    }

    @Override
    public Result doWork() {

        deleteAllData();

        String countryCode = getCountryCodeFromCountry(sharedPrefUtils.getCurrentCountryCode());

        //Common.debugLogger(TAG, "getWorldInData-->" + countryCode);

        if (countryCode != null) {

            try {
                OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(worldURL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .client(httpClient.build())
                        .build();

                OurWorldInDataService service = retrofit.create(OurWorldInDataService.class);
                Call<JsonObject> call = service.getWorldInData();
                Response<JsonObject> response = call.execute();

                if (response != null) {
                    JsonObject jsonObject = response.body();
                    ObjectMapper objectMapper = new ObjectMapper();
                    OurWorldInData ourWorldInData = null;
                    try {
                        ourWorldInData = objectMapper.readValue(jsonObject.getAsJsonObject(countryCode).toString(), OurWorldInData.class);
                    } catch (JsonProcessingException e) {
                        Common.errorLogger(e, e.getMessage(), TAG);
                    }

                    ourWorldInData.setData(ourWorldInData.getData().subList(ourWorldInData.getData().size() - 30, ourWorldInData.getData().size()));

                    WorldData worldData = new WorldData();
                    worldData.setActive(true);
                    worldData.setCountryCode(countryCode);
                    worldData.setCreatedAt(getCurrentDateTime());
                    worldData.setLastRetrievedDate(Common.getDate());
                    worldData.setCountryName(sharedPrefUtils.getCurrentCountry());
                    worldData.setCalculatedPrevalence("0.0");
                    AppDatabaseClient.getInstance(context).getAppDatabase().worldDataDao().insert(worldData);

                    PrevalenceCalculation prevalenceCalculation = new PrevalenceCalculation();
                    prevalenceCalculation.Prevalence(context);
                    Data data = prevalenceCalculation.CalculatePrevalence(ourWorldInData);
                    if (data != null) {
                        return Result.success(data);
                    } else {
                        return Result.failure();
                    }
                }
            } catch (Exception e) {
                errorLogger(e, "getWorldInData", context.getClass().getSimpleName());
                return Result.failure();
            }
        }

        return Result.failure();
    }

    private void deleteAllData() {
        try {
            int isIt14Days = AppDatabaseClient.getInstance(getApplicationContext()).getAppDatabase().riskDao().get28DaysCount();
            if (isIt14Days == 28) {
                List<RiskObject> hashMap=AppDatabaseClient.getInstance(getApplicationContext()).getAppDatabase().riskDao().getLastKnowId();
                long lastKnowId=hashMap.get(0).getId();
                long createdDateStamp=hashMap.get(0).getCreatedDate().getTime();
                AppDatabaseClient.getInstance(getApplicationContext()).getAppDatabase().riskDao().deleteAll(lastKnowId);
                AppDatabaseClient.getInstance(getApplicationContext()).getAppDatabase().regularRiskDao().deleteAll(createdDateStamp);
                AppDatabaseClient.getInstance(getApplicationContext()).getAppDatabase().hourlyRiskDao().deleteAll(createdDateStamp);
                AppDatabaseClient.getInstance(getApplicationContext()).getAppDatabase().dailyRiskDao().deleteAll(createdDateStamp);
                AppDatabaseClient.getInstance(getApplicationContext()).getAppDatabase().scanDao().deleteAll(createdDateStamp);
                AppDatabaseClient.getInstance(getApplicationContext()).getAppDatabase().scanResultsDao().deleteAll(createdDateStamp);
                AppDatabaseClient.getInstance(getApplicationContext()).getAppDatabase().worldDataDao().deleteAll(createdDateStamp);
                AppDatabaseClient.getInstance(getApplicationContext()).getAppDatabase().metaInfoDao().deleteAll(createdDateStamp);
            }
        } catch (Exception e) {
            errorLogger(e, e.getMessage(), TAG);
        }

    }

}
