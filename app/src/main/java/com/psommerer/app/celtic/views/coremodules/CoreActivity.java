package com.psommerer.app.celtic.views.coremodules;

import android.Manifest;
import android.content.Context;

import androidx.core.app.ActivityCompat;

import com.psommerer.app.celtic.R;
import com.psommerer.app.celtic.utils.Common;
import com.psommerer.app.celtic.utils.RuntimePermissionsActivity;


public abstract class CoreActivity extends RuntimePermissionsActivity implements ActivityCompat.OnRequestPermissionsResultCallback {
    private static final String TAG = CoreActivity.class.getSimpleName();
    private static final int REQUEST_PERMISSIONS = 20;
    private Context context;

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    @Override
    public void setContentView(int layoutResID) {
        try {
            super.setContentView(layoutResID);
            bindViews();
            setContext(this);
            setListeners();

        } catch (Exception e) {
            Common.errorLogger(e, e.getMessage(), TAG);
        }
    }


    /**
     * method to bind all views related to resourceLayout
     */
    protected abstract void bindViews();

    /**
     * called to set view listener for views
     */
    protected abstract void setListeners();

    //***************************************************************************************************

    public void isStoragePermissionGranted() {

        super.requestAppPermissions(new
                        String[]{
                        Manifest.permission.ACCESS_NETWORK_STATE,
                        Manifest.permission.CHANGE_NETWORK_STATE,
                        Manifest.permission.INTERNET,
                        Manifest.permission.BLUETOOTH,
                        Manifest.permission.BLUETOOTH_ADMIN,
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.RECEIVE_BOOT_COMPLETED,
                        Manifest.permission.WAKE_LOCK,
                }, R.string
                        .runtime_permissions_txt
                , REQUEST_PERMISSIONS);


    }
    //***************************************************************************************************
//End
}
