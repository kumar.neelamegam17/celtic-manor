package com.psommerer.app.celtic.views.supportedmodules.prevalencecalculation;

import android.content.Context;

import androidx.work.Data;

import com.psommerer.app.celtic.views.supportedmodules.ourworldindata.OurWorldInData;

public interface PrevalenceIF {
    void Prevalence(Context context);

    Data CalculatePrevalence(OurWorldInData ourWorldInData);

    void PersistPrevalence(double prevalence);

    void PersistMetaInfo(Double infectionRate, Double testingRate, Double positivityRate, Double fatalityRate, Double prevalence, Double anonymityRate, Double vaccinationRate);
}
