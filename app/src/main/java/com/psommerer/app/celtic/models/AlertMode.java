package com.psommerer.app.celtic.models;

public enum AlertMode {
    WARNING, ERROR, SUCCESS, RETRY
}
