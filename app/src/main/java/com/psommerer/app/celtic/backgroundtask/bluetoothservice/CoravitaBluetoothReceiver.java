package com.psommerer.app.celtic.backgroundtask.bluetoothservice;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.psommerer.app.celtic.persistence.AppDatabaseClient;
import com.psommerer.app.celtic.persistence.SettingsData.AppSettingsObject;
import com.psommerer.app.celtic.utils.Common;

import java.util.ArrayList;

import no.nordicsemi.android.support.v18.scanner.BluetoothLeScannerCompat;
import no.nordicsemi.android.support.v18.scanner.ScanResult;

import static com.psommerer.app.celtic.utils.Common.debugLogger;
import static com.psommerer.app.celtic.utils.Common.getDistance;

public class CoravitaBluetoothReceiver extends BroadcastReceiver {

    /* ********************************************************************************************** */

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.hasExtra(BluetoothLeScannerCompat.EXTRA_LIST_SCAN_RESULT)) {
            ArrayList<ScanResult> results = intent.getParcelableArrayListExtra(BluetoothLeScannerCompat.EXTRA_LIST_SCAN_RESULT);
            if (results != null) {
                for (final ScanResult result : results) {
                    AppSettingsObject appSettingsObject = AppDatabaseClient.getInstance(context).getAppDatabase().appSettingsDao().getSettings();
                    if (appSettingsObject.getContact_distance() == 5) {
                        if (Double.parseDouble(getDistance(result.getRssi())) >= 4) {
                            Common.addScannedDeviceToList(result.getDevice());
                            debugLogger("scan results:(greater than 4)", result.getRssi() + result.getDevice().getAddress());
                        }
                    } else {
                        if (Double.parseDouble(getDistance(result.getRssi())) <= appSettingsObject.getContact_distance()) {
                            Common.addScannedDeviceToList(result.getDevice());
                            debugLogger("scan results(less than 4):", result.getRssi() + result.getDevice().getAddress());
                        }
                    }
                }
            }
        }
    }

}
