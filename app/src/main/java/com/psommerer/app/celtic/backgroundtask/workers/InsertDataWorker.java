package com.psommerer.app.celtic.backgroundtask.workers;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.psommerer.app.celtic.models.ScanBluetooth;
import com.psommerer.app.celtic.models.ScanStatus;
import com.psommerer.app.celtic.persistence.AppDatabaseClient;
import com.psommerer.app.celtic.persistence.ScanData.ScanObject;
import com.psommerer.app.celtic.persistence.ScanResults.ScanResultsObject;
import com.psommerer.app.celtic.utils.Common;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import static com.psommerer.app.celtic.utils.Common.KEY_COUNTERTAG;
import static com.psommerer.app.celtic.utils.Common.KEY_LATITUDE;
import static com.psommerer.app.celtic.utils.Common.KEY_LONGITUDE;
import static com.psommerer.app.celtic.utils.Common.KEY_SCANNED_DATA;
import static com.psommerer.app.celtic.utils.Common.checkStringEmpty;
import static com.psommerer.app.celtic.utils.Common.errorLogger;
import static com.psommerer.app.celtic.utils.Common.getCurrentDateTime;
import static com.psommerer.app.celtic.utils.Common.runRiskCalculator;

public class InsertDataWorker extends Worker {

    String TAG = InsertDataWorker.class.getSimpleName();
    Context context;

    public InsertDataWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
        this.context = context;
    }

    @NonNull
    @Override
    public Result doWork() {

        try {
            String latitude = getInputData().getString(KEY_LATITUDE);
            String longitude = getInputData().getString(KEY_LONGITUDE);
            int countTag = getInputData().getInt(KEY_COUNTERTAG, 0);
            String scannedListJson = getInputData().getString(KEY_SCANNED_DATA);

            Type listType = new TypeToken<List<ScanBluetooth>>() {
            }.getType();
            List<ScanBluetooth> scannedDevicesList = new Gson().fromJson(scannedListJson, listType);
            List<ScanObject> scanObjectArrayList = new ArrayList<>();

            if (scannedDevicesList != null && scannedDevicesList.size() > 0) {
                for (ScanBluetooth scanBluetooth : scannedDevicesList) {
                    ScanObject scanObject = new ScanObject();
                    scanObject.setCreatedAt(getCurrentDateTime());
                    scanObject.setScanStatus(ScanStatus.SCANNED.name());
                    scanObject.setLatitude(Double.parseDouble(latitude));
                    scanObject.setLongitude(Double.parseDouble(longitude));
                    scanObject.setMacAddress(checkStringEmpty(scanBluetooth.getMacAddress()));
                    scanObject.setActive(true);
                    scanObject.setCountTag(countTag);
                    AppDatabaseClient.getInstance(getApplicationContext()).getAppDatabase().scanDao().insert(scanObject);
                    scanObjectArrayList.add(scanObject);
                }

                Common.debugLogger(TAG, "currentDeviceCount: " + scannedDevicesList.size());
                Common.debugLogger(TAG, "currentCountTag: " + countTag);

            } else {
                /*
                If its empty insert the last know results
                 */
                //List<ScanObject> list = AppDatabaseClient.getInstance(getApplicationContext()).getAppDatabase().scanDao().getLastKnowData();
                //for (ScanObject scanObject : list) {
                    // insert empty scan
                    ScanObject scanObject1 = new ScanObject();
                    scanObject1.setCreatedAt(getCurrentDateTime());
                    scanObject1.setScanStatus(ScanStatus.SCANNED.name());
                    scanObject1.setLatitude(Double.parseDouble(latitude));
                    scanObject1.setLongitude(Double.parseDouble(longitude));
                    scanObject1.setActive(true);
                    scanObject1.setCountTag(countTag);
                    AppDatabaseClient.getInstance(getApplicationContext()).getAppDatabase().scanDao().insert(scanObject1);
                //}

            }

            if (scanObjectArrayList.size() > 0) {
                //insert into scan results
                int currentDeviceCount = AppDatabaseClient.getInstance(getApplicationContext()).getAppDatabase().scanDao().getDeviceCount(countTag);
                Gson gson = new Gson();
                String json = gson.toJson(scanObjectArrayList);
                ScanResultsObject scanResults = new ScanResultsObject();
                scanResults.setActive(true);
                scanResults.setCreatedAt(getCurrentDateTime());
                scanResults.setDeviceCount(currentDeviceCount);
                scanResults.setDataDump(json);
                AppDatabaseClient.getInstance(getApplicationContext()).getAppDatabase().scanResultsDao().insert(scanResults);
            }


            runRiskCalculator(context, countTag);


        } catch (Exception e) {
            errorLogger(e, "Error InsertDataWorker", getApplicationContext().getClass().getSimpleName());
            return Result.failure();
        }
        return Result.success();
    }

    /* ********************************************************************************************** */

}
