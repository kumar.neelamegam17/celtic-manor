package com.psommerer.app.celtic.backgroundtask.workers;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.work.Data;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.psommerer.app.celtic.R;
import com.psommerer.app.celtic.persistence.AppDatabaseClient;
import com.psommerer.app.celtic.persistence.SettingsData.AppSettingsObject;
import com.psommerer.app.celtic.persistence.WorldData.WorldData;
import com.psommerer.app.celtic.utils.Common;
import com.psommerer.app.celtic.utils.SharedPrefUtils;
import com.psommerer.app.celtic.views.supportedmodules.riskcalculation.RiskCalculation;

import org.joda.time.DateTime;

import static com.psommerer.app.celtic.utils.Common.KEY_CURRENTCOUNT_TAG;
import static com.psommerer.app.celtic.utils.Common.convertRiskToInt;
import static com.psommerer.app.celtic.utils.Common.errorLogger;

public class RiskCalculationWorker extends Worker {
    public static String TAG = RiskCalculationWorker.class.getSimpleName();
    Context context;
    WorkManager workManager;

    public RiskCalculationWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
        this.context = context;
    }

    @NonNull
    @Override
    public Result doWork() {

        try {
            SharedPrefUtils sharedPrefUtils = new SharedPrefUtils(getApplicationContext());
            workManager = WorkManager.getInstance(getApplicationContext());
            int currentCTag = getInputData().getInt(KEY_CURRENTCOUNT_TAG, 0);
            //new logic

            WorldData worldData = AppDatabaseClient.getInstance(getApplicationContext()).getAppDatabase().worldDataDao().getLastWorldData();
            String currentPrevalence = worldData.getCalculatedPrevalence();

            int minimumDeviceCount = Common.NO_OF_UNIQUE_DEVICE_COUNT;
            double modifiedPrevalence = Double.parseDouble(currentPrevalence);
            AppSettingsObject appSettingsObject = AppDatabaseClient.getInstance(getApplicationContext()).getAppDatabase().appSettingsDao().getSettings();

            if (appSettingsObject.getCount_interval() == 5) {
                minimumDeviceCount = 1;
                modifiedPrevalence = (Double.parseDouble(currentPrevalence) * 1) / 3;
            } else if (appSettingsObject.getCount_interval() == 10) {
                minimumDeviceCount = 2;
                modifiedPrevalence = (Double.parseDouble(currentPrevalence) * 2) / 3;
            } else if (appSettingsObject.getCount_interval() == 15) {
                minimumDeviceCount = 3;
                modifiedPrevalence = Double.parseDouble(currentPrevalence);
            }

            int deviceCount;
            if (currentCTag == 0) {//old concept to calculate risk every 1 hour
                Common.debugLogger(TAG, "1hr CALCULATED PREVALENCE (P): " + currentPrevalence);
                deviceCount = AppDatabaseClient.getInstance(getApplicationContext()).getAppDatabase().scanDao().getTotalDeviceCountInLastScan(minimumDeviceCount);
                Common.debugLogger(TAG, "1hr DEVICE COUNT (N): " + deviceCount);
                AppDatabaseClient.getInstance(getApplicationContext()).getAppDatabase().scanDao().updateAllActiveStatus();
            } else {//new concept to calculate risk every 5minutes
                DateTime dateTime = new DateTime();
                deviceCount = AppDatabaseClient.getInstance(getApplicationContext()).getAppDatabase().scanDao().getDeviceCountOfLast1Hour((dateTime.minusHours(1).toDate().getTime()), (dateTime.minusHours(0).toDate().getTime()), minimumDeviceCount);
            }

            RiskCalculation riskCalculation = new RiskCalculation();
            riskCalculation.RiskCalculation(context, currentCTag);
            String value = riskCalculation.CalculateRisk(modifiedPrevalence, deviceCount);
            Common.debugLogger(TAG, "CALCULATED RISK: " + value);

            if (sharedPrefUtils.getNotificationStatus()) {
                int riskValue = convertRiskToInt(value);
                notificationHourlyLogic(context, riskValue);
            }


        } catch (Exception e) {
            errorLogger(e, "RiskCalculationWorker", TAG);
            return Result.failure();
        }

        return Result.success();
    }

    public void notificationHourlyLogic(Context context, int riskValue) {
        AppSettingsObject appSettingsObject = AppDatabaseClient.getInstance(context).getAppDatabase().appSettingsDao().getSettings();
        if (appSettingsObject != null) {
            if (appSettingsObject.isNotificationEnabled() && appSettingsObject.getNotificationInterval().equals(context.getString(R.string.hourly_risk))) {
                if (appSettingsObject.getNotificationLevel().equalsIgnoreCase(Common.checkRiskScalesString(riskValue, context))) {

                    Data myData = new Data.Builder()
                            .putInt(AlertNotificationWorker.KEY_RISK_VALUE, riskValue)
                            .putInt(AlertNotificationWorker.KEY_RISK_TYPE, 1)
                            .build();

                    OneTimeWorkRequest hourlyWork = new OneTimeWorkRequest.Builder(AlertNotificationWorker.class)
                            .addTag(Common.WORKER_TAG_HOURLYNOTIFICATION)
                            .setInputData(myData)
                            .build();
                    workManager.enqueue(hourlyWork);

                }
            }
        }
    }



}
