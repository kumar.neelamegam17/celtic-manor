package com.psommerer.app.celtic.persistence;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.psommerer.app.celtic.persistence.MetaInfo.MetaInfoDao;
import com.psommerer.app.celtic.persistence.MetaInfo.MetaInfoObject;
import com.psommerer.app.celtic.persistence.RiskData.RegularRiskDao;
import com.psommerer.app.celtic.persistence.RiskData.RegularRiskObject;
import com.psommerer.app.celtic.persistence.RiskData.RiskDao;
import com.psommerer.app.celtic.persistence.RiskData.RiskObject;
import com.psommerer.app.celtic.persistence.ScanData.ScanDao;
import com.psommerer.app.celtic.persistence.ScanData.ScanObject;
import com.psommerer.app.celtic.persistence.ScanResults.ScanResultsDao;
import com.psommerer.app.celtic.persistence.ScanResults.ScanResultsObject;
import com.psommerer.app.celtic.persistence.SettingsData.AppSettingsDao;
import com.psommerer.app.celtic.persistence.SettingsData.AppSettingsObject;
import com.psommerer.app.celtic.persistence.Statistics.DailyRiskDao;
import com.psommerer.app.celtic.persistence.Statistics.DailyRiskObject;
import com.psommerer.app.celtic.persistence.Statistics.HourlyRiskDao;
import com.psommerer.app.celtic.persistence.Statistics.HourlyRiskObject;
import com.psommerer.app.celtic.persistence.WorldData.WorldData;
import com.psommerer.app.celtic.persistence.WorldData.WorldDataDao;

@Database(entities = {ScanObject.class, ScanResultsObject.class, RiskObject.class, RegularRiskObject.class, AppSettingsObject.class, WorldData.class, MetaInfoObject.class, HourlyRiskObject.class, DailyRiskObject.class}, version = 2, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {
    public abstract ScanDao scanDao();

    public abstract ScanResultsDao scanResultsDao();

    public abstract RiskDao riskDao();

    public abstract RegularRiskDao regularRiskDao();

    public abstract WorldDataDao worldDataDao();

    public abstract AppSettingsDao appSettingsDao();

    public abstract MetaInfoDao metaInfoDao();

    public abstract HourlyRiskDao hourlyRiskDao();

    public abstract DailyRiskDao dailyRiskDao();

}

