package com.psommerer.app.celtic.models;

public enum MajorDeviceClass {
    PHONE, UNCATEGORIZED, WEARABLE, MISC, HEALTH, AUDIO_VIDEO
}
