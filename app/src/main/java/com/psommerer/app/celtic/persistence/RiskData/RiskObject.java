package com.psommerer.app.celtic.persistence.RiskData;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.psommerer.app.celtic.persistence.DateConverter;

import java.io.Serializable;
import java.util.Date;

@Entity
public class RiskObject implements Serializable {
    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "isactive")
    private boolean isActive;

    @ColumnInfo(name = "created_at")
    @TypeConverters({DateConverter.class})
    private Date createdAt;

    @ColumnInfo(name = "riskValue")
    private double riskValueDouble;

    private String riskValueString;

    private int calculatedDeviceCount;

    private double calculatedPrevalence;

    @TypeConverters({DateConverter.class})
    private Date createdTime;

    @TypeConverters({DateConverter.class})
    private Date createdDate;

    private String createdTimeStr;
    private int riskValueInt;

    public String getCreatedTimeStr() {
        return createdTimeStr;
    }

    public void setCreatedTimeStr(String createdTimeStr) {
        this.createdTimeStr = createdTimeStr;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public double getCalculatedPrevalence() {
        return calculatedPrevalence;
    }

    public void setCalculatedPrevalence(double calculatedPrevalence) {
        this.calculatedPrevalence = calculatedPrevalence;
    }

    public int getCalculatedDeviceCount() {
        return calculatedDeviceCount;
    }

    public void setCalculatedDeviceCount(int calculatedDeviceCount) {
        this.calculatedDeviceCount = calculatedDeviceCount;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public double getRiskValueDouble() {
        return riskValueDouble;
    }

    public void setRiskValueDouble(double riskValueDouble) {
        this.riskValueDouble = riskValueDouble;
    }

    public String getRiskValueString() {
        return riskValueString;
    }

    public void setRiskValueString(String riskValueString) {
        this.riskValueString = riskValueString;
    }

    public int getRiskValueInt() {
        return riskValueInt;
    }

    public void setRiskValueInt(int riskValueInt) {
        this.riskValueInt = riskValueInt;
    }
}
