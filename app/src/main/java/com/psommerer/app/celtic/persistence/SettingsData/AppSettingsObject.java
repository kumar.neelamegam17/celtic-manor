package com.psommerer.app.celtic.persistence.SettingsData;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.psommerer.app.celtic.persistence.DateConverter;

import java.io.Serializable;
import java.util.Date;

@Entity
public class AppSettingsObject implements Serializable {
    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "isactive")
    private boolean isActive;

    @ColumnInfo(name = "created_at")
    @TypeConverters({DateConverter.class})
    private Date createdAt;

    @ColumnInfo(name = "notification_enabled")
    private boolean notificationEnabled;

    @ColumnInfo(name = "notification_level")
    private String notificationLevel;

    @ColumnInfo(name = "selected_language")
    private String language;

    private String notificationInterval;

    private int contact_distance;
    private int count_interval;
    private int notificationType;

    public int getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(int notificationType) {
        this.notificationType = notificationType;
    }

    public int getContact_distance() {
        return contact_distance;
    }

    public void setContact_distance(int contact_distance) {
        this.contact_distance = contact_distance;
    }

    public int getCount_interval() {
        return count_interval;
    }

    public void setCount_interval(int count_interval) {
        this.count_interval = count_interval;
    }

    public String getNotificationInterval() {
        return notificationInterval;
    }

    public void setNotificationInterval(String notificationInterval) {
        this.notificationInterval = notificationInterval;
    }


    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getNotificationLevel() {
        return notificationLevel;
    }

    public void setNotificationLevel(String notificationLevel) {
        this.notificationLevel = notificationLevel;
    }

    public boolean isNotificationEnabled() {
        return notificationEnabled;
    }

    public void setNotificationEnabled(boolean notificationEnabled) {
        this.notificationEnabled = notificationEnabled;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }
}
