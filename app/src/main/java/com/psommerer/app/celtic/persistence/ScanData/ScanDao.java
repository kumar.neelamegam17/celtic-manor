package com.psommerer.app.celtic.persistence.ScanData;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface ScanDao {
    @Query("SELECT * FROM ScanObject WHERE isactive=1")
    List<ScanObject> getAll();

    @Query("SELECT COUNT(DISTINCT mac_address) as count FROM ScanObject WHERE  countTag=:countTag AND isactive=1")
    int getDeviceCount(int countTag);

    @Insert
    void insert(ScanObject task);

    @Delete
    void delete(ScanObject task);

    @Update
    void update(ScanObject task);

    @Query("UPDATE ScanObject set isactive=0 and countTag=0 WHERE isactive=1")
    void updateAllActiveStatus();

    @Query("SELECT SUM(t.count) as count FROM (SELECT count(DISTINCT mac_address)as count FROM ScanObject WHERE isactive=1 GROUP BY mac_address HAVING COUNT(mac_address) >=  :uniqueDeviceCount) as t")
    int getTotalDeviceCountInLastScan(int uniqueDeviceCount);

    @Query("SELECT SUM(t.count) as count FROM (SELECT count(DISTINCT mac_address)as count FROM ScanObject WHERE isactive=1 and countTag=:currentCountTag GROUP BY mac_address) as t")
    int getLastDeviceCountInLastScan(int currentCountTag);

    @Query("SELECT SUM(t.count) as count FROM (SELECT count(DISTINCT mac_address)as count FROM ScanObject WHERE created_at BETWEEN :from and :to GROUP BY mac_address HAVING COUNT(mac_address) >=  :uniqueDeviceCount) as t limit 100")
    int getDeviceCountOfLast1Hour(long from, long to, int uniqueDeviceCount);

    @Query("SELECT SUM(t.count) as count FROM (SELECT count(DISTINCT mac_address)as count FROM ScanObject WHERE created_at BETWEEN :from and :to GROUP BY mac_address HAVING COUNT(mac_address) >=  :uniqueDeviceCount) as t limit 500")
    int getDeviceCountOfLast1Day(long from, long to, int uniqueDeviceCount);

    @Query("SELECT * FROM ScanObject WHERE created_at BETWEEN :from and :to GROUP BY mac_address")
    List<ScanObject> getInfo(long from, long to);

    @Query("SELECT * FROM ScanObject WHERE Id=(SELECT MAX(Id)  FROM ScanObject WHERE isactive=1)")
    ScanObject getLastScanInfo();

    @Query("DELETE FROM ScanObject where created_at<:date")
    void deleteAll(long date);

    @Query("select * from ScanObject where isactive=1 and mac_address!=''  and countTag=(select countTag from ScanObject where isactive=1 order by Id desc limit 1) order by Id DESC")
    List<ScanObject> getLastKnowData();


}
