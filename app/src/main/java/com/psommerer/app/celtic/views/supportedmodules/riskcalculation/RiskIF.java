package com.psommerer.app.celtic.views.supportedmodules.riskcalculation;

import android.content.Context;

public interface RiskIF {
    void RiskCalculation(Context context, int currentCTag);

    String CalculateRisk(double prevalenceP, int noOfDevicesN);

    void PersistRiskHourly(String riskValue, double prevalenceP, int noOfDevicesN);

}
