package com.psommerer.app.celtic.views.coremodules;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.Settings;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.LiveData;
import androidx.work.Constraints;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkInfo;
import androidx.work.WorkManager;

import com.psommerer.app.celtic.R;
import com.psommerer.app.celtic.backgroundtask.bluetoothservice.CoravitaBluetoothService;
import com.psommerer.app.celtic.backgroundtask.coravitaservices.CoravitaReceiver;
import com.psommerer.app.celtic.backgroundtask.workers.PrevalenceDataWorker;
import com.psommerer.app.celtic.databinding.ActivityMainBinding;
import com.psommerer.app.celtic.models.AlertMode;
import com.psommerer.app.celtic.persistence.AppDatabaseClient;
import com.psommerer.app.celtic.persistence.MetaInfo.MetaInfoObject;
import com.psommerer.app.celtic.persistence.WorldData.WorldData;
import com.psommerer.app.celtic.utils.Common;
import com.psommerer.app.celtic.utils.SharedPrefUtils;
import com.psommerer.app.celtic.views.submodules.AppSettingsActivity;
import com.psommerer.app.celtic.views.submodules.MoreInfoActivity;
import com.psommerer.app.celtic.views.submodules.StatisticsActivity;
import com.psommerer.app.celtic.views.supportedmodules.BottomSheets;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.karumi.dexter.BuildConfig;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import java.util.List;

import static com.akexorcist.localizationactivity.core.LanguageSetting.setLanguage;
import static com.psommerer.app.celtic.utils.Common.LOW_RISK_THRESHOLD;
import static com.psommerer.app.celtic.utils.Common.checkMetaInfoIsValid;
import static com.psommerer.app.celtic.utils.Common.checkMetaInfoVaccination;
import static com.psommerer.app.celtic.utils.Common.debugLogger;
import static com.psommerer.app.celtic.utils.Common.errorLogger;
import static com.psommerer.app.celtic.utils.Common.getAppLocale;
import static com.psommerer.app.celtic.utils.Common.setUpTheme;


public class MainActivity extends AppCompatActivity {

    /* ********************************************************************************************** */
    public static final String TAG = MainActivity.class.getSimpleName();
    /**
     * GPS NEW CODE
     */
    private static final int REQUEST_CHECK_SETTINGS = 100;
    private final BottomSheets bottomSheets = new BottomSheets(this);
    Dialog bottomDialog;
    /* ********************************************************************************************** */
    //Bluetooth
    private BluetoothAdapter bAdapter = null;
    private ActivityMainBinding activityMainBinding;
    private SharedPrefUtils sharedPrefUtils;
    /* ********************************************************************************************** */
    private FusedLocationProviderClient mFusedLocationClient;
    private SettingsClient mSettingsClient;
    private LocationRequest mLocationRequest;
    private LocationSettingsRequest mLocationSettingsRequest;
    /* ********************************************************************************************** */
    private LocationCallback mLocationCallback;
    private Location mCurrentLocation;
    /* ********************************************************************************************** */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setLanguage(this, getAppLocale());

        try {
            activityMainBinding = ActivityMainBinding.inflate(getLayoutInflater());
            View view = activityMainBinding.getRoot();
            setUpTheme(this);
            setContentView(view);
            initComponents();
        } catch (Exception e) {
            errorLogger(e, "onCreate: ", TAG);
        }
    }

    /* ********************************************************************************************** */
    private void initComponents() {
        sharedPrefUtils = SharedPrefUtils.getInstance(this);
        initiateBluetoothEnabled();//Check the bluetooth connection
        if (!sharedPrefUtils.getServiceStatus()) {
            CoravitaReceiver.setAlarm(this, Common.ALARM_SERVICE_EXECUTION_INTERVAL_IN_MS_5);
            debugLogger(TAG, "Cora Service started");
            callScan();
            sharedPrefUtils.setServiceStatus(true);
        }

        bottomDialog = new Dialog(this, R.style.BottomDialog);
        loadRiskValue();
        MetaInfoObject metaInfoObject = AppDatabaseClient.getInstance(this).getAppDatabase().metaInfoDao().getLastMetaInfoDao();
        if (metaInfoObject != null) {
            loadMetaInfos(metaInfoObject);
            activityMainBinding.currentcasesLayout.errorinput.setVisibility(View.GONE);
        } else {
            activityMainBinding.currentcasesLayout.errorinput.setVisibility(View.VISIBLE);
            activityMainBinding.currentcasesLayout.errorinput.setText(getString(R.string.nosufficientdata));
        }

        //Experiment.testHourlyNotification(this);
    }

    private void callScan() {
        Intent intent = new Intent(getApplicationContext(), CoravitaBluetoothService.class);
        debugLogger(TAG, "Cora BluetoothService: started");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForegroundService(intent);
        } else {
            startService(intent);
        }

    }

    /* ********************************************************************************************** */

    public void loadMetaInfo() {
        Handler handler = new Handler(Looper.myLooper()) {
            @Override
            public void handleMessage(Message msg) {
                MetaInfoObject metaInfoObject = (MetaInfoObject) msg.obj;
                if (metaInfoObject != null) {
                    loadMetaInfos(metaInfoObject);
                    activityMainBinding.currentcasesLayout.errorinput.setVisibility(View.GONE);
                } else {
                    showCustomDialog(getString(R.string.informationtitle), getResources().getString(R.string.nodatatrylater), AlertMode.ERROR);
                    activityMainBinding.currentcasesLayout.errorinput.setVisibility(View.VISIBLE);
                    activityMainBinding.currentcasesLayout.errorinput.setText(getString(R.string.nosufficientdata));
                }
                sharedPrefUtils.setHardLoad(false);
            }
        };

        Runnable runnable = () -> {
            Message msg = handler.obtainMessage();
            msg.obj = AppDatabaseClient.getInstance(this).getAppDatabase().metaInfoDao().getLastMetaInfoDao();
            handler.sendMessage(msg);
        };
        Thread riskThread = new Thread(runnable);
        riskThread.start();
    }

    private void loadMetaInfos(MetaInfoObject metaInfoObject) {
        try {
            activityMainBinding.currentcasesLayout.metainfoInfectionrate.setText(checkMetaInfoIsValid(metaInfoObject.getInfectionRate()));
            activityMainBinding.currentcasesLayout.metainfoPositivityrate.setText(checkMetaInfoIsValid(metaInfoObject.getPositivityRate()));
            activityMainBinding.currentcasesLayout.metainfoFatalityrate.setText(checkMetaInfoIsValid(metaInfoObject.getFatalityRate()));
            activityMainBinding.currentcasesLayout.metainfoPrevalencerate.setText(Common.convertPrevalenceData(metaInfoObject.getPrevalenceRate()));
            activityMainBinding.currentcasesLayout.metainfoAnonymityrate.setText(checkMetaInfoIsValid(metaInfoObject.getAnonymityRate()));
            if (metaInfoObject.getVaccinationRate() != null) {
                activityMainBinding.currentcasesLayout.metainfoVaccinationrate.setText(checkMetaInfoVaccination(metaInfoObject.getVaccinationRate()));
                activityMainBinding.currentcasesLayout.layTesting.setVisibility(View.GONE);
                activityMainBinding.currentcasesLayout.layVaccination.setVisibility(View.VISIBLE);
            } else {
                activityMainBinding.currentcasesLayout.metainfoTestingrate.setText(checkMetaInfoIsValid(metaInfoObject.getTestingRate()));
                activityMainBinding.currentcasesLayout.layVaccination.setVisibility(View.GONE);
                activityMainBinding.currentcasesLayout.layTesting.setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            Common.errorLogger(e, e.getMessage(), TAG);
        }
    }

    /* ********************************************************************************************** */
    private void hardLoad() {
        if (sharedPrefUtils.getHardLoad()) {

            Constraints myConstraints = new Constraints.Builder().setRequiredNetworkType(NetworkType.CONNECTED).build();
            OneTimeWorkRequest runWorldDataRetrievalWorker = new OneTimeWorkRequest.Builder(PrevalenceDataWorker.class).setConstraints(myConstraints)
                    .addTag(Common.WORKER_TAG_PREVALENCECALCULATION)
                    .build();
            WorkManager.getInstance(getApplicationContext()).enqueue(runWorldDataRetrievalWorker);

            LiveData<WorkInfo> status = WorkManager.getInstance(getApplicationContext()).getWorkInfoByIdLiveData(runWorldDataRetrievalWorker.getId());
            status.observe(this, workStatus -> {
                switch (workStatus.getState()) {
                    case FAILED:
                        debugLogger(TAG, "A Status is failed");
                        dismissBottomSheet();
                        loadMetaInfo();
                        Toast.makeText(getApplicationContext(), R.string.nosufficientdata, Toast.LENGTH_LONG).show();
                        break;
                    case RUNNING:
                        debugLogger(TAG, "A Status is running");
                        showBottomSheet();
                        break;
                    case SUCCEEDED:
                        debugLogger(TAG, "A Status is succeeded");
                        dismissBottomSheet();
                        loadMetaInfo();
                        break;
                    default:
                        debugLogger(TAG, "A Status is unknown");
                }
            });

        } else {
            MetaInfoObject metaInfoObject = AppDatabaseClient.getInstance(this).getAppDatabase().metaInfoDao().getLastMetaInfoDao();
            if (metaInfoObject != null) {
                loadMetaInfos(metaInfoObject);
                activityMainBinding.currentcasesLayout.errorinput.setVisibility(View.GONE);
            } else {
                activityMainBinding.currentcasesLayout.errorinput.setVisibility(View.VISIBLE);
                activityMainBinding.currentcasesLayout.errorinput.setText(getString(R.string.nosufficientdata));
            }
        }
    }

    private void showBottomSheet() {
        View contentView = LayoutInflater.from(this).inflate(R.layout.bottom_sheet_layout, null);
        bottomDialog.setContentView(contentView);
        bottomDialog.setCancelable(false);
        ViewGroup.LayoutParams layoutParams = contentView.getLayoutParams();
        layoutParams.width = getResources().getDisplayMetrics().widthPixels;
        contentView.setLayoutParams(layoutParams);
        ImageView imageView = contentView.findViewById(R.id.loading_img);
        RotateAnimation rotate = new RotateAnimation(
                0, 360,
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f
        );
        rotate.setDuration(900);
        rotate.setRepeatCount(Animation.INFINITE);
        imageView.startAnimation(rotate);
        bottomDialog.getWindow().setGravity(Gravity.BOTTOM);
        bottomDialog.getWindow().setWindowAnimations(R.style.BottomDialog_Animation);
        if (!bottomDialog.isShowing()) {
            bottomDialog.show();
        }
    }

    private void dismissBottomSheet() {
        if (bottomDialog.isShowing()) {
            bottomDialog.dismiss();
        }
    }

    private void loadRiskValue() {
        Integer riskValue = AppDatabaseClient.getInstance(getApplicationContext()).getAppDatabase().regularRiskDao().getLastRiskValue();
        if (riskValue != null && riskValue > LOW_RISK_THRESHOLD) {
            activityMainBinding.riskvalue.setText(String.valueOf(riskValue));
            updateRiskText(riskValue);
            activityMainBinding.lessthan.setVisibility(View.GONE);
        } else {
            activityMainBinding.riskvalue.setText(String.valueOf(LOW_RISK_THRESHOLD));
            updateRiskText(LOW_RISK_THRESHOLD);
            activityMainBinding.lessthan.setVisibility(View.VISIBLE);
        }
    }

    private void updateRiskText(int riskValue) {

        int riskResult = Common.checkRiskScalesInt(riskValue);
        switch (riskResult) {
            case 2:
                activityMainBinding.riskText.setText(getText(R.string.moderate_risk_sl));
                activityMainBinding.riskBackground.setImageResource(R.drawable.risk_moderate);
                activityMainBinding.coraaColorScale.moderateriskLayout.setBackground(ContextCompat.getDrawable(this, R.drawable.selected_aqi_foreground));
                break;

            case 3:
                activityMainBinding.riskText.setText(getText(R.string.high_risk_sl));
                activityMainBinding.riskBackground.setImageResource(R.drawable.risk_high);
                activityMainBinding.coraaColorScale.hihgriskLayout.setBackground(ContextCompat.getDrawable(this, R.drawable.selected_aqi_foreground));
                break;

            case 4:
                activityMainBinding.riskText.setText(getText(R.string.veryhigh_risk_sl));
                activityMainBinding.riskBackground.setImageResource(R.drawable.risk_veryhigh);
                activityMainBinding.coraaColorScale.veryhighriskLayout.setBackground(ContextCompat.getDrawable(this, R.drawable.selected_aqi_foreground));
                break;

            default:
                activityMainBinding.riskText.setText(getText(R.string.low_risk_sl));
                activityMainBinding.riskBackground.setImageResource(R.drawable.risk_low);
                activityMainBinding.coraaColorScale.lowriskLayout.setBackground(ContextCompat.getDrawable(this, R.drawable.selected_aqi_foreground));
        }

    }

    private void initiateBluetoothEnabled() {
        bAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bAdapter == null) {
            Toast.makeText(getApplicationContext(), R.string.nobluetoothsupport, Toast.LENGTH_SHORT).show();
        } else {
            if (!bAdapter.isEnabled()) {
                bAdapter.enable();
            }
        }
    }

    /* ********************************************************************************************** */
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void updateTheme(View view) {
        SharedPrefUtils sharedPrefUtils = SharedPrefUtils.getInstance(this);
        sharedPrefUtils.isDarkMode(!sharedPrefUtils.isDarkMode());
        recreate();
    }

    public void performRefresh(View view) {
        Common.showToast("Info updated..", Toast.LENGTH_SHORT, view.getContext());
        initComponents();
    }

    public void showStatistics(View view) {
        startActivity(new Intent(this, StatisticsActivity.class));
    }

    public void showMetaDataInfo(View view) {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.metadatanote);
        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        AppCompatButton dButton = dialog.findViewById(R.id.dialog_close);

        dButton.setOnClickListener(v -> dialog.dismiss());

        if (!dialog.isShowing()) {
            dialog.show();
            dialog.getWindow().setAttributes(lp);
        }

    }

    /* ********************************************************************************************** */

    public void showSettings(View view) {
        startActivity(new Intent(this, AppSettingsActivity.class));
    }

    public void showShare(View view1) {
        showShareBottomSheetDialog();
    }

    public void showMoreInfo(View view) {
        startActivity(new Intent(this, MoreInfoActivity.class));
    }

    @Override
    protected void onStop() {
        super.onStop();
        System.gc();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        System.gc();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }


    /* ********************************************************************************************** */
    private void showShareBottomSheetDialog() {
        bottomSheets.showShare();
    }

    /* ********************************************************************************************** */
    public void showCustomDialog(String title, String message, AlertMode alertMode) {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.commondialog);
        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        AppCompatButton dButton = dialog.findViewById(R.id.dialog_bt_close);
        LinearLayoutCompat dLayout = dialog.findViewById(R.id.dialog_bg);
        AppCompatTextView dTitle = dialog.findViewById(R.id.dialog_title);
        AppCompatTextView dMessage = dialog.findViewById(R.id.dialog_content);
        AppCompatImageView dIcon = dialog.findViewById(R.id.dialog_icon);

        if (AlertMode.WARNING == alertMode) {
            dLayout.setBackground(ContextCompat.getDrawable(this, R.color.orange_400));
            dIcon.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.vec_warning));
        } else if (AlertMode.SUCCESS == alertMode) {
            dLayout.setBackground(ContextCompat.getDrawable(this, R.color.green_400));
            dIcon.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.vec_success));
        } else if (AlertMode.ERROR == alertMode) {
            dLayout.setBackground(ContextCompat.getDrawable(this, R.color.red_400));
            dIcon.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.vec_error));
        } else if (AlertMode.RETRY == alertMode) {
            dLayout.setBackground(ContextCompat.getDrawable(this, R.color.yellow_400));
            dIcon.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.vec_retry));
        }

        dTitle.setText(title);
        dMessage.setText(message);
        dButton.setOnClickListener(v -> dialog.dismiss());

        if (!dialog.isShowing()) {
            dialog.show();
            dialog.getWindow().setAttributes(lp);
        }

    }

    @Override
    protected void onResume() {
        try {
            if (checkPermissions()) {
                initLocation();
            }
        } catch (Exception e) {
            Common.errorLogger(e, e.getMessage(), TAG);
        }
        super.onResume();
    }

    private void initLocation() {

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mSettingsClient = LocationServices.getSettingsClient(this);

        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                // location is received
                mCurrentLocation = locationResult.getLastLocation();
                updateLocationUI();

            }
        };

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(Common.LOCATION_INTERVAL);
        mLocationRequest.setFastestInterval(Common.LOCATION_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();

        // Requesting ACCESS_FINE_LOCATION using Dexter library
        Dexter.withContext(getApplicationContext())
                .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        startLocationUpdates();
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        if (response.isPermanentlyDenied()) {
                            openSettings();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();


    }

    @SuppressLint("MissingPermission")
    private void startLocationUpdates() {
        mSettingsClient
                .checkLocationSettings(mLocationSettingsRequest)
                .addOnSuccessListener(this, locationSettingsResponse -> {
                    mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                            mLocationCallback, Looper.myLooper());
                    updateLocationUI();
                })
                .addOnFailureListener(this, e -> {
                    int statusCode = ((ApiException) e).getStatusCode();
                    switch (statusCode) {
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            try {
                                ResolvableApiException rae = (ResolvableApiException) e;
                                rae.startResolutionForResult(MainActivity.this, REQUEST_CHECK_SETTINGS);
                            } catch (IntentSender.SendIntentException sie) {
                                Common.errorLogger(e, e.getMessage(), TAG);
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            String errorMessage = "Location settings are inadequate, and cannot be fixed here. Fix in Settings.";
                            Toast.makeText(MainActivity.this, errorMessage, Toast.LENGTH_LONG).show();
                    }

                    updateLocationUI();
                });
    }


    private void updateLocationUI() {

        if (mCurrentLocation == null) {
            return;
        }
        sharedPrefUtils.setCurrentLatitude(mCurrentLocation.getLatitude());
        sharedPrefUtils.setCurrentLongitude(mCurrentLocation.getLongitude());
        Geocoder geocoder = new Geocoder(getApplicationContext(), Common.getAppLocale());
        try {
            List<Address> addresses = geocoder.getFromLocation(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude(), 1);
            String locality;
            String country;
            StringBuilder stringBuilder = new StringBuilder();
            if (addresses.get(0).getLocality() != null) {
                locality = addresses.get(0).getLocality();
                stringBuilder.append(locality).append(",");
            }

            if (addresses.get(0).getCountryName() != null) {
                country = addresses.get(0).getCountryName();
                stringBuilder.append(country);
            }
            String currentAddress = stringBuilder.toString();
            activityMainBinding.locationTextView.setText(currentAddress);
            sharedPrefUtils.setCurrentCountryCode(addresses.get(0).getCountryCode());
            sharedPrefUtils.setCurrentCountry(addresses.get(0).getCountryName());
            sharedPrefUtils.setCurrentCity(addresses.get(0).getLocality());

            String countryCode = addresses.get(0).getCountryCode();
            WorldData worldData = AppDatabaseClient.getInstance(getApplicationContext()).getAppDatabase().worldDataDao().getLastWorldData();
            if (worldData == null) {
                if (sharedPrefUtils.getNetworkAvailability() && countryCode != null) {
                    hardLoad();
                } else {
                    showCustomDialog("Information", getString(R.string.internet_not_available), AlertMode.WARNING);
                }
            }

        } catch (Exception e) {
            Common.errorLogger(e, e.getMessage(), TAG);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            // Check for the integer request code originally supplied to startResolutionForResult().
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        //Log.e(TAG, "User agreed to make required location settings changes.");
                        break;
                    case Activity.RESULT_CANCELED:
                        //Log.e(TAG, "User chose not to make required location settings changes.");
                        break;
                }
                break;
        }
    }

    private void openSettings() {
        Intent intent = new Intent();
        intent.setAction(
                Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package",
                BuildConfig.LIBRARY_PACKAGE_NAME, null);
        intent.setData(uri);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }


}