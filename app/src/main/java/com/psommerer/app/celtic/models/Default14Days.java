package com.psommerer.app.celtic.models;

import java.util.Date;

public class Default14Days {
    int Id;
    Date newDay;
    String dateStr;

    public Default14Days(int id, Date newDay, String dateStr) {
        Id = id;
        this.newDay = newDay;
        this.dateStr = dateStr;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public Date getNewDay() {
        return newDay;
    }

    public void setNewDay(Date newDay) {
        this.newDay = newDay;
    }

    public String getDateStr() {
        return dateStr;
    }

    public void setDateStr(String dateStr) {
        this.dateStr = dateStr;
    }
}
