package com.psommerer.app.celtic.models;

public class DefaultTimes {

    int Id;
    String startTime;
    String endTime;

    public DefaultTimes(int id, String startTime, String endTime) {
        Id = id;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
}
