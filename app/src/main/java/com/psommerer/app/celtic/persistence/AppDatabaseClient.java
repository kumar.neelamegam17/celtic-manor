package com.psommerer.app.celtic.persistence;

import android.content.Context;

import androidx.room.Room;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;

public class AppDatabaseClient {
    private static AppDatabaseClient mInstance;
    private final AppDatabase appDatabase;

    private AppDatabaseClient(Context mCtx) {

        //add allow on main thread
        appDatabase = Room.databaseBuilder(mCtx, AppDatabase.class, "CORA").allowMainThreadQueries().addMigrations(MIGRATION_1_2).build();

    }

    public static synchronized AppDatabaseClient getInstance(Context mCtx) {
        if (mInstance == null) {
            mInstance = new AppDatabaseClient(mCtx);
        }
        return mInstance;
    }

    public AppDatabase getAppDatabase() {
        return appDatabase;
    }

    static final Migration MIGRATION_1_2 = new Migration(1, 2) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE DailyRiskObject  ADD COLUMN latitude STRING NOT NULL DEFAULT 0");
            database.execSQL("ALTER TABLE DailyRiskObject  ADD COLUMN longitude STRING NOT NULL DEFAULT 0");

            database.execSQL("ALTER TABLE HourlyRiskObject  ADD COLUMN latitude STRING NOT NULL DEFAULT 0");
            database.execSQL("ALTER TABLE HourlyRiskObject  ADD COLUMN longitude STRING NOT NULL DEFAULT 0");
        }
    };

}
