package com.psommerer.app.celtic.views.supportedmodules.riskcalculation;

import android.content.Context;

import com.psommerer.app.celtic.persistence.AppDatabaseClient;
import com.psommerer.app.celtic.persistence.RiskData.RegularRiskObject;
import com.psommerer.app.celtic.persistence.RiskData.RiskObject;
import com.psommerer.app.celtic.persistence.Statistics.HourlyRiskObject;
import com.psommerer.app.celtic.utils.Common;
import com.psommerer.app.celtic.utils.SharedPrefUtils;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Date;

import static com.psommerer.app.celtic.utils.Common.convertRiskToInt;
import static com.psommerer.app.celtic.utils.Common.getCurrentDateTime;
import static com.psommerer.app.celtic.utils.Common.getOnlyDateWOTZ;
import static com.psommerer.app.celtic.utils.Common.getOnlyTimeWOTZ;
import static com.psommerer.app.celtic.utils.Common.getTimeString;

public class RiskCalculation implements RiskIF {

    Context context;
    String TAG = RiskCalculation.class.getSimpleName();
    int currentCTag;

    @Override
    public void RiskCalculation(Context context, int currentCTag) {
        this.context = context;
        this.currentCTag = currentCTag;
    }

    @Override
    public String CalculateRisk(double prevalenceP, int noOfDevicesN) {
        int X = 1;
        BigDecimal equalResult = Utils.equal(prevalenceP, noOfDevicesN, X);
        BigDecimal greaterResult = Utils.greater(prevalenceP, noOfDevicesN, X);
        BigDecimal greaterEqualResult = equalResult.add(greaterResult);
        //Common.debugLogger(TAG, "Calculated Binomial Distribution Risk = " + Utils.getPrettyNum(greaterEqualResult));
        PersistRiskHourly(Utils.getPrettyNum(greaterEqualResult), prevalenceP, noOfDevicesN);
        return Utils.getPrettyNum(greaterEqualResult);
    }

    @Override
    public void PersistRiskHourly(String riskValue, double prevalenceP, int noOfDevicesN) {
        new Thread(() -> {
            try {
                Date currentDateTime = getCurrentDateTime();
                SharedPrefUtils sharedPrefUtils=new SharedPrefUtils(context);
                //TODO get old data
                if (currentCTag == 0) { //insert only during 1 hour cycle
                    RiskObject riskObject = new RiskObject();
                    riskObject.setActive(true);
                    riskObject.setCreatedAt(currentDateTime);
                    riskObject.setCreatedTime(getOnlyTimeWOTZ(currentDateTime));
                    riskObject.setCreatedTimeStr(getTimeString(currentDateTime));
                    riskObject.setCreatedDate(getOnlyDateWOTZ(currentDateTime));
                    riskObject.setRiskValueDouble(Common.roundFromString(riskValue, 2));
                    riskObject.setRiskValueInt(convertRiskToInt(riskValue));//multiple x100 for making integer
                    riskObject.setRiskValueString(riskValue);
                    riskObject.setCalculatedPrevalence(prevalenceP);
                    riskObject.setCalculatedDeviceCount(noOfDevicesN);
                    AppDatabaseClient.getInstance(context).getAppDatabase().riskDao().insert(riskObject);

                    HourlyRiskObject hourlyRiskObject = new HourlyRiskObject();
                    hourlyRiskObject.setActive(true);
                    hourlyRiskObject.setCreatedAt(currentDateTime);
                    hourlyRiskObject.setCreatedTime(getOnlyTimeWOTZ(currentDateTime));
                    hourlyRiskObject.setCreatedDate(getOnlyDateWOTZ(currentDateTime));
                    hourlyRiskObject.setRiskValueInt(convertRiskToInt(riskValue));
                    hourlyRiskObject.setLatitude(sharedPrefUtils.getCurrentLatitude());
                    hourlyRiskObject.setLongitude(sharedPrefUtils.getCurrentLongitude());
                    AppDatabaseClient.getInstance(context).getAppDatabase().hourlyRiskDao().insert(hourlyRiskObject);
                    Common.debugLogger(TAG, "Persist Risk Hourly: " + Common.roundFromString(riskValue, 2) + "--//-" + convertRiskToInt(riskValue) + "--//-" + riskValue);

                } else { //insert every 5 minutes
                    RegularRiskObject regularRiskObject = new RegularRiskObject();
                    regularRiskObject.setActive(true);
                    regularRiskObject.setCreatedAt(currentDateTime);
                    regularRiskObject.setCreatedTime(getOnlyTimeWOTZ(currentDateTime));
                    regularRiskObject.setCreatedTimeStr(getTimeString(currentDateTime));
                    regularRiskObject.setCreatedDate(getOnlyDateWOTZ(currentDateTime));
                    regularRiskObject.setRiskValueDouble(Common.roundFromString(riskValue, 2));
                    regularRiskObject.setRiskValueInt(convertRiskToInt(riskValue));//multiple x100 for making integer
                    regularRiskObject.setRiskValueString(riskValue);
                    regularRiskObject.setCalculatedPrevalence(prevalenceP);
                    regularRiskObject.setCalculatedDeviceCount(noOfDevicesN);
                    AppDatabaseClient.getInstance(context).getAppDatabase().regularRiskDao().insert(regularRiskObject);
                }

            } catch (ParseException e) {
                Common.errorLogger(e, e.getMessage(), TAG);
            }
        }).start();
    }

}
