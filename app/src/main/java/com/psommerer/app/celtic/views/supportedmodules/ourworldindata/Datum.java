package com.psommerer.app.celtic.views.supportedmodules.ourworldindata;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "date",
        "total_cases",
        "new_cases",
        "new_cases_smoothed",
        "total_deaths",
        "new_deaths",
        "new_deaths_smoothed",
        "total_cases_per_million",
        "new_cases_per_million",
        "new_cases_smoothed_per_million",
        "total_deaths_per_million",
        "new_deaths_per_million",
        "new_deaths_smoothed_per_million",
        "total_tests",
        "new_tests",
        "total_tests_per_thousand",
        "new_tests_per_thousand",
        "new_tests_smoothed",
        "new_tests_smoothed_per_thousand",
        "tests_per_case",
        "positive_rate",
        "tests_units",
        "total_vaccinations",
        "total_vaccinations_per_hundred"
})
public class Datum {

    @JsonIgnore
    private final Map<String, Object> additionalProperties = new HashMap<String, Object>();
    @JsonProperty("date")
    private String date;
    @JsonProperty("total_cases")
    private Integer totalCases;
    @JsonProperty("new_cases")
    private Integer newCases;
    @JsonProperty("new_cases_smoothed")
    private Double newCasesSmoothed;
    @JsonProperty("total_deaths")
    private Integer totalDeaths;
    @JsonProperty("new_deaths")
    private Integer newDeaths;
    @JsonProperty("new_deaths_smoothed")
    private Double newDeathsSmoothed;
    @JsonProperty("total_cases_per_million")
    private Double totalCasesPerMillion;
    @JsonProperty("new_cases_per_million")
    private Double newCasesPerMillion;
    @JsonProperty("new_cases_smoothed_per_million")
    private Double newCasesSmoothedPerMillion;
    @JsonProperty("total_deaths_per_million")
    private Double totalDeathsPerMillion;
    @JsonProperty("new_deaths_per_million")
    private Double newDeathsPerMillion;
    @JsonProperty("new_deaths_smoothed_per_million")
    private Double newDeathsSmoothedPerMillion;
    @JsonProperty("total_tests")
    private Integer totalTests;
    @JsonProperty("new_tests")
    private Integer newTests;
    @JsonProperty("total_tests_per_thousand")
    private Double totalTestsPerThousand;
    @JsonProperty("new_tests_per_thousand")
    private Double newTestsPerThousand;
    @JsonProperty("new_tests_smoothed")
    private Integer newTestsSmoothed;
    @JsonProperty("new_tests_smoothed_per_thousand")
    private Double newTestsSmoothedPerThousand;
    @JsonProperty("tests_per_case")
    private Double testsPerCase;
    @JsonProperty("positive_rate")
    private Double positiveRate;
    @JsonProperty("tests_units")
    private String testsUnits;
    @JsonProperty("total_vaccinations")
    private Double totalVaccinations;
    @JsonProperty("total_vaccinations_per_hundred")
    private Double totalVaccinationsPerHundred;

    @JsonProperty("total_vaccinations")
    public Double getTotalVaccinations() {
        return totalVaccinations;
    }

    @JsonProperty("total_vaccinations")
    public void setTotalVaccinations(Double totalVaccinations) {
        this.totalVaccinations = totalVaccinations;
    }

    @JsonProperty("total_vaccinations_per_hundred")
    public Double getTotalVaccinationsPerHundred() {
        return totalVaccinationsPerHundred;
    }

    @JsonProperty("total_vaccinations_per_hundred")
    public void setTotalVaccinationsPerHundred(Double totalVaccinationsPerHundred) {
        this.totalVaccinationsPerHundred = totalVaccinationsPerHundred;
    }

    @JsonProperty("date")
    public String getDate() {
        return date;
    }

    @JsonProperty("date")
    public void setDate(String date) {
        this.date = date;
    }

    @JsonProperty("total_cases")
    public Integer getTotalCases() {
        return totalCases;
    }

    @JsonProperty("total_cases")
    public void setTotalCases(Integer totalCases) {
        this.totalCases = totalCases;
    }

    @JsonProperty("new_cases")
    public Integer getNewCases() {
        return newCases;
    }

    @JsonProperty("new_cases")
    public void setNewCases(Integer newCases) {
        this.newCases = newCases;
    }

    @JsonProperty("new_cases_smoothed")
    public Double getNewCasesSmoothed() {
        return newCasesSmoothed;
    }

    @JsonProperty("new_cases_smoothed")
    public void setNewCasesSmoothed(Double newCasesSmoothed) {
        this.newCasesSmoothed = newCasesSmoothed;
    }

    @JsonProperty("total_deaths")
    public Integer getTotalDeaths() {
        return totalDeaths;
    }

    @JsonProperty("total_deaths")
    public void setTotalDeaths(Integer totalDeaths) {
        this.totalDeaths = totalDeaths;
    }

    @JsonProperty("new_deaths")
    public Integer getNewDeaths() {
        return newDeaths;
    }

    @JsonProperty("new_deaths")
    public void setNewDeaths(Integer newDeaths) {
        this.newDeaths = newDeaths;
    }

    @JsonProperty("new_deaths_smoothed")
    public Double getNewDeathsSmoothed() {
        return newDeathsSmoothed;
    }

    @JsonProperty("new_deaths_smoothed")
    public void setNewDeathsSmoothed(Double newDeathsSmoothed) {
        this.newDeathsSmoothed = newDeathsSmoothed;
    }

    @JsonProperty("total_cases_per_million")
    public Double getTotalCasesPerMillion() {
        return totalCasesPerMillion;
    }

    @JsonProperty("total_cases_per_million")
    public void setTotalCasesPerMillion(Double totalCasesPerMillion) {
        this.totalCasesPerMillion = totalCasesPerMillion;
    }

    @JsonProperty("new_cases_per_million")
    public Double getNewCasesPerMillion() {
        return newCasesPerMillion;
    }

    @JsonProperty("new_cases_per_million")
    public void setNewCasesPerMillion(Double newCasesPerMillion) {
        this.newCasesPerMillion = newCasesPerMillion;
    }

    @JsonProperty("new_cases_smoothed_per_million")
    public Double getNewCasesSmoothedPerMillion() {
        return newCasesSmoothedPerMillion;
    }

    @JsonProperty("new_cases_smoothed_per_million")
    public void setNewCasesSmoothedPerMillion(Double newCasesSmoothedPerMillion) {
        this.newCasesSmoothedPerMillion = newCasesSmoothedPerMillion;
    }

    @JsonProperty("total_deaths_per_million")
    public Double getTotalDeathsPerMillion() {
        return totalDeathsPerMillion;
    }

    @JsonProperty("total_deaths_per_million")
    public void setTotalDeathsPerMillion(Double totalDeathsPerMillion) {
        this.totalDeathsPerMillion = totalDeathsPerMillion;
    }

    @JsonProperty("new_deaths_per_million")
    public Double getNewDeathsPerMillion() {
        return newDeathsPerMillion;
    }

    @JsonProperty("new_deaths_per_million")
    public void setNewDeathsPerMillion(Double newDeathsPerMillion) {
        this.newDeathsPerMillion = newDeathsPerMillion;
    }

    @JsonProperty("new_deaths_smoothed_per_million")
    public Double getNewDeathsSmoothedPerMillion() {
        return newDeathsSmoothedPerMillion;
    }

    @JsonProperty("new_deaths_smoothed_per_million")
    public void setNewDeathsSmoothedPerMillion(Double newDeathsSmoothedPerMillion) {
        this.newDeathsSmoothedPerMillion = newDeathsSmoothedPerMillion;
    }

    @JsonProperty("total_tests")
    public Integer getTotalTests() {
        return totalTests;
    }

    @JsonProperty("total_tests")
    public void setTotalTests(Integer totalTests) {
        this.totalTests = totalTests;
    }

    @JsonProperty("new_tests")
    public Integer getNewTests() {
        return newTests;
    }

    @JsonProperty("new_tests")
    public void setNewTests(Integer newTests) {
        this.newTests = newTests;
    }

    @JsonProperty("total_tests_per_thousand")
    public Double getTotalTestsPerThousand() {
        return totalTestsPerThousand;
    }

    @JsonProperty("total_tests_per_thousand")
    public void setTotalTestsPerThousand(Double totalTestsPerThousand) {
        this.totalTestsPerThousand = totalTestsPerThousand;
    }

    @JsonProperty("new_tests_per_thousand")
    public Double getNewTestsPerThousand() {
        return newTestsPerThousand;
    }

    @JsonProperty("new_tests_per_thousand")
    public void setNewTestsPerThousand(Double newTestsPerThousand) {
        this.newTestsPerThousand = newTestsPerThousand;
    }

    @JsonProperty("new_tests_smoothed")
    public Integer getNewTestsSmoothed() {
        return newTestsSmoothed;
    }

    @JsonProperty("new_tests_smoothed")
    public void setNewTestsSmoothed(Integer newTestsSmoothed) {
        this.newTestsSmoothed = newTestsSmoothed;
    }

    @JsonProperty("new_tests_smoothed_per_thousand")
    public Double getNewTestsSmoothedPerThousand() {
        return newTestsSmoothedPerThousand;
    }

    @JsonProperty("new_tests_smoothed_per_thousand")
    public void setNewTestsSmoothedPerThousand(Double newTestsSmoothedPerThousand) {
        this.newTestsSmoothedPerThousand = newTestsSmoothedPerThousand;
    }

    @JsonProperty("tests_per_case")
    public Double getTestsPerCase() {
        return testsPerCase;
    }

    @JsonProperty("tests_per_case")
    public void setTestsPerCase(Double testsPerCase) {
        this.testsPerCase = testsPerCase;
    }

    @JsonProperty("positive_rate")
    public Double getPositiveRate() {
        return positiveRate;
    }

    @JsonProperty("positive_rate")
    public void setPositiveRate(Double positiveRate) {
        this.positiveRate = positiveRate;
    }

    @JsonProperty("tests_units")
    public String getTestsUnits() {
        return testsUnits;
    }

    @JsonProperty("tests_units")
    public void setTestsUnits(String testsUnits) {
        this.testsUnits = testsUnits;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}