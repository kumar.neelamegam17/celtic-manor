package com.psommerer.app.celtic.persistence.Statistics;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Ignore;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface HourlyRiskDao {
    @Query("SELECT * FROM HourlyRiskObject")
    List<HourlyRiskObject> getAll();

    @Insert
    void insert(HourlyRiskObject task);

    @Delete
    void delete(HourlyRiskObject task);

    @Update
    void update(HourlyRiskObject task);

    @Ignore
    @Query("SELECT id,isActive,riskValueInt,created_at,createdTime,createdDate,latitude,longitude FROM HourlyRiskObject WHERE isactive=1 AND created_at BETWEEN :from and :to")
    List<HourlyRiskObject> getLast24HoursData(long from, long to);

    @Query("DELETE FROM HourlyRiskObject where createdDate<:date")
    void deleteAll(long date);

}
