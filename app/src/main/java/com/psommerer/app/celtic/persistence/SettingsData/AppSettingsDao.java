package com.psommerer.app.celtic.persistence.SettingsData;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface AppSettingsDao {
    @Query("SELECT * FROM AppSettingsObject")
    List<AppSettingsObject> getAll();

    @Insert
    void insert(AppSettingsObject task);

    @Delete
    void delete(AppSettingsObject task);

    @Update
    void update(AppSettingsObject task);

    @Query("DELETE FROM AppSettingsObject")
    void deleteAll();

    @Query("SELECT * FROM AppSettingsObject WHERE Id=(SELECT MAX(Id)  FROM AppSettingsObject WHERE isactive=1)")
    AppSettingsObject getSettings();

}
