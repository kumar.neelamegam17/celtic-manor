package com.psommerer.app.celtic.persistence.RiskData;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Ignore;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.RoomWarnings;
import androidx.room.Update;

import java.util.List;

@Dao
public interface RiskDao {
    @Query("SELECT * FROM RiskObject")
    List<RiskObject> getAll();

    @Insert
    void insert(RiskObject task);

    @Delete
    void delete(RiskObject task);

    @Update
    void update(RiskObject task);

    @Query("SELECT (CASE WHEN riskValueInt < 1 THEN 1 ELSE riskValueInt END)as risk FROM RiskObject WHERE Id = (SELECT MAX(Id)  FROM RiskObject);")
    int getLastRiskValue();

    @Query("SELECT * FROM RiskObject WHERE isactive=1 AND created_at BETWEEN :from and :to")
    List<RiskObject> getLast24HoursData(long from, long to);


    @Query("SELECT createdDate,createdTimeStr, createdTime, id,isActive,riskValue,riskValueInt, riskValueString,calculatedPrevalence,sum(calculatedDeviceCount)as calculatedDeviceCount, created_at FROM RiskObject GROUP BY date(createdDate/1000, 'unixepoch')")
    List<RiskObject> getDailyData();


    @Query("SELECT count(t.a) as result FROM  (select count(id)as a from RiskObject GROUP BY date(createdDate/1000, 'unixepoch')) as t")
    int get28DaysCount();

    @SuppressWarnings(RoomWarnings.CURSOR_MISMATCH)
    @Ignore
    @Query("SELECT min(t.id) as id, * FROM  (select count(id)as a, * from RiskObject GROUP BY date(createdDate/1000, 'unixepoch') order by id desc limit 14) as t")
    List<RiskObject> getLastKnowId();

    @Query("DELETE FROM RiskObject where id<:lastKnowId")
    void deleteAll(long lastKnowId);


}
