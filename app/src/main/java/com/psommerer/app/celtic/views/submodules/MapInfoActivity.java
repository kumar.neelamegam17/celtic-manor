package com.psommerer.app.celtic.views.submodules;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.DrawableRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NavUtils;
import androidx.core.content.ContextCompat;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.psommerer.app.celtic.R;
import com.psommerer.app.celtic.databinding.ActivityMapsBinding;
import com.psommerer.app.celtic.persistence.Statistics.DailyRiskObject;
import com.psommerer.app.celtic.persistence.Statistics.HourlyRiskObject;
import com.psommerer.app.celtic.utils.Common;
import com.psommerer.app.celtic.utils.SharedPrefUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MapInfoActivity extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    List<DailyRiskObject> customDailyBarObjects = new ArrayList<>();
    List<HourlyRiskObject> customHourlyBarObjects = new ArrayList<>();
    private int riskMapFlag=1;//1: HOURLY 2: DAILY
    ActivityMapsBinding activityMapsBinding;
    SupportMapFragment mapFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityMapsBinding = ActivityMapsBinding.inflate(getLayoutInflater());
        View view = activityMapsBinding.getRoot();
        setContentView(view);
        setUpActionBar();
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    private void setUpActionBar() {
        setUpTheme();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle(getResources().getString(R.string.riskmap_activity_title));
    }

    //**********************************************************************************************
    public void setUpTheme() {
        SharedPrefUtils sharedPrefUtils = SharedPrefUtils.getInstance(this);
        if (sharedPrefUtils.isDarkMode()) {
            setTheme(R.style.AppTheme_Dark);
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorthemeDarkPrimary)));
        } else {
            setTheme(R.style.AppTheme_Light);
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorthemeLightPrimary)));
        }
    }
    //**********************************************************************************************

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;
        mMap.clear();
        if(riskMapFlag==1)//Hourly risk map
        {
            customHourlyBarObjects = this.getIntent().getParcelableArrayListExtra("hourlylist");
            int i=0;
            for (HourlyRiskObject customBarObject : customHourlyBarObjects) {
                if(customBarObject.isActive()){
                    if(customBarObject.getLatitude()!=null && customBarObject.getLongitude()!=null){
                        int riskResult = Common.checkRiskScalesInt(customBarObject.getRiskValueInt());
                        double latitude_val= Double.parseDouble(customBarObject.getLatitude());
                        double longitude_val= Double.parseDouble(customBarObject.getLongitude());
                        addMarkerToMap(customBarObject.getRiskValueInt(), riskResult, latitude_val, longitude_val);
                        if(i==customHourlyBarObjects.size()){
                            mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(latitude_val, longitude_val)));
                        }
                    }
                }
                i++;
            }

        }else {
            customDailyBarObjects = this.getIntent().getParcelableArrayListExtra("dailylist");
            int i=0;
            for (DailyRiskObject customBarObject : customDailyBarObjects) {
                if(customBarObject.isActive()){
                    if(customBarObject.getLatitude()!=null && customBarObject.getLongitude()!=null){
                        int riskResult = Common.checkRiskScalesInt(customBarObject.getRiskValueInt());
                        double latitude_val= Double.parseDouble(customBarObject.getLatitude());
                        double longitude_val= Double.parseDouble(customBarObject.getLongitude());
                        addMarkerToMap(customBarObject.getRiskValueInt(), riskResult, latitude_val, longitude_val);
                        if(i==customDailyBarObjects.size()){
                            mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(latitude_val, longitude_val)));
                        }
                    }
                }
                i++;
            }
        }

        mMap.getUiSettings().setZoomControlsEnabled(true);
        //setMapLongClick(mMap);
        //setPoiClick(mMap);
    }

    private void addMarkerToMap(int riskIntValue, int riskResult, double latitude_val, double longitude_val) {
        switch (riskResult) {
            case 2:
                mMap.addMarker(new MarkerOptions().icon(bitmapDescriptorFromVector(this, R.drawable.marker_yellow)).position(new LatLng(latitude_val, longitude_val)).title(String.valueOf(riskIntValue)));
                break;

            case 3:
                mMap.addMarker(new MarkerOptions().icon(bitmapDescriptorFromVector(this, R.drawable.marker_orange)).position(new LatLng(latitude_val, longitude_val)).title(String.valueOf(riskIntValue)));
                break;

            case 4:
                mMap.addMarker(new MarkerOptions().icon(bitmapDescriptorFromVector(this, R.drawable.marker_red)).position(new LatLng(latitude_val, longitude_val)).title(String.valueOf(riskIntValue)));
                break;

            default:
                mMap.addMarker(new MarkerOptions().icon(bitmapDescriptorFromVector(this, R.drawable.marker_green)).position(new LatLng(latitude_val, longitude_val)).title(String.valueOf(riskIntValue)));
        }
    }

    private BitmapDescriptor bitmapDescriptorFromVector(Context context, @DrawableRes int vectorDrawableResourceId) {
        Drawable background = ContextCompat.getDrawable(context, vectorDrawableResourceId);
        background.setBounds(0, 0, background.getIntrinsicWidth(), background.getIntrinsicHeight());
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorDrawableResourceId);
        vectorDrawable.setBounds(40, 20, vectorDrawable.getIntrinsicWidth() + 40, vectorDrawable.getIntrinsicHeight() + 20);
        Bitmap bitmap = Bitmap.createBitmap(background.getIntrinsicWidth(), background.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        background.draw(canvas);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    private void setMapLongClick(final GoogleMap map) {
        map.setOnMapLongClickListener(latLng -> {
            String snippet = String.format(Locale.getDefault(),
                    "Lat: %1$.5f, Long: %2$.5f",
                    latLng.latitude,
                    latLng.longitude);

            map.addMarker(new MarkerOptions()
                    .position(latLng)
                    .title("Marker in Sydney")
                    .snippet(snippet));
        });
    }

    private void setPoiClick(final GoogleMap map) {

        map.setOnPoiClickListener(poi -> {
            Marker poiMarker = mMap.addMarker(new MarkerOptions()
                    .position(poi.latLng)
                    .title(poi.name));
            poiMarker.showInfoWindow();
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.map_options, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Change the map type based on the user's selection.
        Toast.makeText(getApplicationContext(), R.string.updated, Toast.LENGTH_LONG).show();
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            case R.id.hourlyrisk:
                riskMapFlag=1;
                activityMapsBinding.mapTitleHint.setText(getString(R.string.last_24_hours_risk_data));
                mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
                mapFragment.getMapAsync(this);
                return true;
            case R.id.dailyrisk:
                riskMapFlag=2;
                activityMapsBinding.mapTitleHint.setText(getString(R.string.last_14_days_risk_data));
                mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
                mapFragment.getMapAsync(this);
                return true;
            case R.id.normal_map:
                mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                return true;
            case R.id.hybrid_map:
                mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                return true;
            case R.id.satellite_map:
                mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                return true;
            case R.id.terrain_map:
                mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //**********************************************************************************************
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

}