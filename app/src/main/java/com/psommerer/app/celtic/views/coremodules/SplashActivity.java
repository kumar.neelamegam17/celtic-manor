package com.psommerer.app.celtic.views.coremodules;


import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatButton;

import com.psommerer.app.celtic.R;
import com.psommerer.app.celtic.databinding.ActivitySplashBinding;
import com.psommerer.app.celtic.persistence.AppDatabaseClient;
import com.psommerer.app.celtic.persistence.SettingsData.AppSettingsObject;
import com.psommerer.app.celtic.utils.CheckNetwork;
import com.psommerer.app.celtic.utils.Common;
import com.psommerer.app.celtic.utils.SharedPrefUtils;
import com.github.ybq.android.spinkit.sprite.Sprite;
import com.github.ybq.android.spinkit.style.Wave;
import com.google.firebase.FirebaseApp;
import com.google.firebase.crashlytics.FirebaseCrashlytics;

import static com.akexorcist.localizationactivity.core.LanguageSetting.setLanguage;
import static com.psommerer.app.celtic.utils.Common.DEFAULT_CONTACT_DISTANCE;
import static com.psommerer.app.celtic.utils.Common.DEFAULT_SCAN_INTERVAL;
import static com.psommerer.app.celtic.utils.Common.doBounceAnimation;
import static com.psommerer.app.celtic.utils.Common.getAppLocale;
import static com.psommerer.app.celtic.utils.Common.getCurrentDateTime;

public class SplashActivity extends CoreActivity {

    private static final long SPLASH_DURATION = 3000;
    SharedPrefUtils sharedPrefUtils;
    ActivitySplashBinding activitySplashBinding;
    String TAG = SplashActivity.class.getSimpleName();

    //**********************************************************************************************
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setLanguage(this, getAppLocale());

        sharedPrefUtils = SharedPrefUtils.getInstance(this);
        if (sharedPrefUtils.isDarkMode()) setTheme(R.style.AppTheme_Dark);
        else setTheme(R.style.AppTheme_Light);
        activitySplashBinding = ActivitySplashBinding.inflate(getLayoutInflater());
        View view = activitySplashBinding.getRoot();
        setContentView(view);

        initiateDashboard();


    }

    void initiateDashboard() {

        try {
            if (!sharedPrefUtils.getDisclaimer()) {
                callDisclaimer();
            } else {
                isStoragePermissionGranted();
            }

        } catch (Exception e) {
            Common.errorLogger(e, e.getMessage(), TAG);
        }
    }

    private void callDisclaimer() {

        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.disclaimer);
        dialog.setCancelable(false);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;


        WebView webView = dialog.findViewById(R.id.dataview);
        webView.loadData(getResources().getString(R.string.disclaimer), "text/html", "UTF-8");

        AppCompatButton dButton = dialog.findViewById(R.id.dialog_close);
        dButton.setOnClickListener(v -> {
            dialog.dismiss();
            isStoragePermissionGranted();
            sharedPrefUtils.setDisclaimer(true);
        });

        if (!dialog.isShowing()) {
            dialog.show();
            dialog.getWindow().setAttributes(lp);
        }
    }

    //**********************************************************************************************
    @Override
    public void onPermissionsGranted(int requestCode) {

        try {
            getInit();
        } catch (Exception e) {
            Common.errorLogger(e, e.getMessage(), TAG);
        }

    }

    //**********************************************************************************************
    private void getInit() {

        Sprite animate = new Wave();
        activitySplashBinding.progressBar.setIndeterminateDrawable(animate);
        doBounceAnimation(activitySplashBinding.imageView, 2);
        CheckNetwork checkNetwork = new CheckNetwork(this);
        checkNetwork.registerNetworkCallback();
        sharedPrefUtils.setHardLoad(true);

        final BluetoothAdapter bAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bAdapter == null) {
            Toast.makeText(getApplicationContext(), R.string.nobluetoothsupport, Toast.LENGTH_SHORT).show();
        } else {
            if (!bAdapter.isEnabled()) {
                bAdapter.enable();
            }
            new Handler(Looper.myLooper()).postDelayed(() -> navigate(), SPLASH_DURATION);
        }

        FirebaseApp.initializeApp(getApplicationContext());
        FirebaseCrashlytics.getInstance().setCrashlyticsCollectionEnabled(true);
        insertDefaultSettings();

    }

    //**********************************************************************************************

    private void insertDefaultSettings() {

        AppSettingsObject status = AppDatabaseClient.getInstance(this).getAppDatabase().appSettingsDao().getSettings();
        if (status == null) {
            new Thread(() -> {
                AppSettingsObject appSettingsObject = new AppSettingsObject();
                appSettingsObject.setCreatedAt(getCurrentDateTime());
                appSettingsObject.setActive(true);
                appSettingsObject.setLanguage(this.getString(R.string.language_english));
                appSettingsObject.setNotificationEnabled(true);
                appSettingsObject.setNotificationLevel(getString(R.string.moderate_risk));
                appSettingsObject.setNotificationInterval(getString(R.string.hourly_risk));
                appSettingsObject.setNotificationType(1);
                appSettingsObject.setContact_distance(DEFAULT_CONTACT_DISTANCE);
                appSettingsObject.setCount_interval(DEFAULT_SCAN_INTERVAL);
                AppDatabaseClient.getInstance(this).getAppDatabase().appSettingsDao().deleteAll();
                AppDatabaseClient.getInstance(this).getAppDatabase().appSettingsDao().insert(appSettingsObject);
            }).start();
        }
    }

    //**********************************************************************************************
    @Override
    protected void onResume() {
        super.onResume();
    }

    private void navigate() {
        startActivity(new Intent(SplashActivity.this, MainActivity.class));
        finishAffinity();
    }

    //**********************************************************************************************
    @Override
    public void onBackPressed() {

    }

    //**********************************************************************************************
    @Override
    protected void bindViews() {

    }

    //**********************************************************************************************
    @Override
    protected void setListeners() {

    }

    //**********************************************************************************************
    @Override
    protected void onPause() {
        super.onPause();
    }

    //**********************************************************************************************
    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    //**********************************************************************************************
    @Override
    protected void onStop() {
        super.onStop();
    }

//**********************************************************************************************
}
