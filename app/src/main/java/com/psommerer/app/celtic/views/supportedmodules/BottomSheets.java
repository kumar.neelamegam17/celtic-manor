package com.psommerer.app.celtic.views.supportedmodules;

import android.content.Intent;

import com.psommerer.app.celtic.R;
import com.psommerer.app.celtic.utils.Common;
import com.psommerer.app.celtic.views.coremodules.MainActivity;

public class BottomSheets {

    private final MainActivity mainActivity;
    String TAG = BottomSheets.class.getSimpleName();

    public BottomSheets(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    /* ********************************************************************************************** */

    public void showShare() {
        try {
            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, mainActivity.getString(R.string.app_name));
            String shareMessage = mainActivity.getResources().getString(R.string.share_message);
            //shareMessage = String.format(Common.getAppLocale(), mainActivity.getResources().getString(R.string.applink), shareMessage, BuildConfig.APPLICATION_ID);
            shareMessage = String.format(Common.getAppLocale(), "%s%s", mainActivity.getResources().getString(R.string.weblink), shareMessage);
            shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
            mainActivity.startActivity(Intent.createChooser(shareIntent, mainActivity.getResources().getString(R.string.choooseone)));
        } catch (Exception e) {
            Common.errorLogger(e, "showShare", TAG);
        }

    }

    /* ********************************************************************************************** */


}