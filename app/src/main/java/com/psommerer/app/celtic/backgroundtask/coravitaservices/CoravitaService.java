package com.psommerer.app.celtic.backgroundtask.coravitaservices;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.widget.Toast;

import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;

import com.psommerer.app.celtic.backgroundtask.bluetoothservice.CoravitaBluetoothService;
import com.psommerer.app.celtic.backgroundtask.newservice.RestartJobIntentService;
import com.psommerer.app.celtic.backgroundtask.workers.DailyNotificationWorker;
import com.psommerer.app.celtic.persistence.AppDatabaseClient;
import com.psommerer.app.celtic.persistence.Statistics.DailyRiskObject;
import com.psommerer.app.celtic.persistence.WorldData.WorldData;
import com.psommerer.app.celtic.utils.Common;
import com.psommerer.app.celtic.utils.SharedPrefUtils;

import org.joda.time.LocalTime;

import java.text.ParseException;
import java.util.Date;

import static com.psommerer.app.celtic.utils.Common.DAILYNOTIFICATION_FLAG;
import static com.psommerer.app.celtic.utils.Common.WORKER_TAG_DAILYNOTIFICATION;
import static com.psommerer.app.celtic.utils.Common.convertRiskToInt;
import static com.psommerer.app.celtic.utils.Common.debugLogger;
import static com.psommerer.app.celtic.utils.Common.errorLogger;
import static com.psommerer.app.celtic.utils.Common.getCurrentDateTime;
import static com.psommerer.app.celtic.utils.Common.getOnlyDateWOTZ;
import static com.psommerer.app.celtic.utils.Common.getOnlyTimeWOTZ;
import static com.psommerer.app.celtic.utils.Common.getRiskValueForLast24Hours;
import static com.psommerer.app.celtic.utils.Common.insertScanDataWorker;
import static com.psommerer.app.celtic.utils.Common.isMyServiceRunning;

public class CoravitaService extends Service {
    /* ********************************************************************************************** */
    String TAG = CoravitaService.class.getSimpleName();
    SharedPrefUtils sharedPrefUtils;

    /* ********************************************************************************************** */
    @Override
    public void onCreate() {
        Common.debugLogger(TAG, "Service OnCreate");
        sharedPrefUtils = new SharedPrefUtils(getApplicationContext());
    }

    /* ********************************************************************************************** */

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        BluetoothAdapter bAdapter = BluetoothAdapter.getDefaultAdapter();
        if (!bAdapter.isEnabled()) {
            bAdapter.enable();
        }

        int currentCountTag=sharedPrefUtils.getCurrentCountTag();
        Common.debugLogger(TAG, "Service onStartCommand");
        Common.debugLogger(TAG, "onStartCommand: COUNT_TAG - " + currentCountTag);
        if (currentCountTag <= Common.NUMBER_OF_SCANNING_ITERATION) {
            insertScanDataWorker(getApplicationContext(), sharedPrefUtils.getCurrentLatitude(), sharedPrefUtils.getCurrentLongitude(), currentCountTag);
            currentCountTag = currentCountTag + 1;
            sharedPrefUtils.setCurrentCountTag(currentCountTag);
        } else {
            if (currentCountTag == Common.NUMBER_OF_SCANNING_ITERATION + 1) {
                Common.debugLogger(TAG, "onStartCommand: restart the alarm" + (Common.NUMBER_OF_SCANNING_ITERATION + 1) + " round");
                Common.runRiskCalculator(getApplicationContext(), 0);
                sharedPrefUtils.setCurrentCountTag(1);
                try {
                    WorldData worldData = AppDatabaseClient.getInstance(getApplicationContext()).getAppDatabase().worldDataDao().getLastWorldData();
                    if (worldData == null || worldData.getCreatedAt().before(getOnlyDateWOTZ(getCurrentDateTime()))) {
                        runWorldInData();
                        sharedPrefUtils.setNotificationStatus(true);
                        restartService();
                    }
                    DailyRiskObject dailyRiskObject = AppDatabaseClient.getInstance(getApplicationContext()).getAppDatabase().dailyRiskDao().getLastDailyRisk();
                    if (dailyRiskObject == null || dailyRiskObject.getCreatedAt().before(getOnlyDateWOTZ(getCurrentDateTime()))) {
                        insertDailyData();
                        DAILYNOTIFICATION_FLAG = false;
                    }
                    } catch (ParseException e) {
                    errorLogger(e, e.getMessage(), TAG);
                }
            }
        }


        if (!DAILYNOTIFICATION_FLAG) {
            if (sharedPrefUtils.getNotificationStatus()) {
                LocalTime time1 = new LocalTime("09:00:00");
                LocalTime time2 = new LocalTime("09:15:00");
                LocalTime time3 = new LocalTime();
                if (time1.compareTo(time3) * time3.compareTo(time2) > 0) {
                    Common.debugLogger(TAG, WORKER_TAG_DAILYNOTIFICATION);
                    OneTimeWorkRequest dailyWorker = new OneTimeWorkRequest.Builder(DailyNotificationWorker.class)
                            .addTag(Common.WORKER_TAG_HOURLYNOTIFICATION)
                            .build();
                    WorkManager.getInstance(getApplicationContext()).enqueue(dailyWorker);
                }
            }
        }

        return START_STICKY;
    }

    /* ********************************************************************************************** */
    public void restartService(){
        CoravitaReceiver.setAlarm(this, Common.ALARM_SERVICE_EXECUTION_INTERVAL_IN_MS_5);
        debugLogger(TAG, "Cora Service restarted");
        Intent intent = new Intent(getApplicationContext(), CoravitaBluetoothService.class);
        debugLogger(TAG, "Cora BluetoothService: restarted");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForegroundService(intent);
        } else {
            startService(intent);
        }
        Toast.makeText(this, "Service restarted successfully...", Toast.LENGTH_SHORT).show();
    }
    /* ********************************************************************************************** */

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    /**
     * get our world in data
     * run risk calculator
     * insert scanned data
     * schedule widget updater
     */
    public void runWorldInData() {
        Common.runOurWorldData(getApplicationContext());
    }

    public void insertDailyData() throws ParseException {
        Date currentDateTime = getCurrentDateTime();
        String calculated1DayRisk = getRiskValueForLast24Hours(getApplicationContext());
        DailyRiskObject dailyRiskObject = new DailyRiskObject();
        dailyRiskObject.setActive(true);
        dailyRiskObject.setLatitude(sharedPrefUtils.getCurrentLatitude());
        dailyRiskObject.setLongitude(sharedPrefUtils.getCurrentLongitude());
        dailyRiskObject.setCreatedAt(currentDateTime);
        dailyRiskObject.setCreatedTime(getOnlyTimeWOTZ(currentDateTime));
        dailyRiskObject.setCreatedDate(getOnlyDateWOTZ(currentDateTime));
        dailyRiskObject.setRiskValueInt(convertRiskToInt(calculated1DayRisk));
        AppDatabaseClient.getInstance(getApplicationContext()).getAppDatabase().dailyRiskDao().insert(dailyRiskObject);
    }

    /* ********************************************************************************************** */

    @Override
    public void onDestroy() {
        super.onDestroy();
        Common.debugLogger(TAG, "onDestroy");
        callDozeService();
    }
    /* ********************************************************************************************** */

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
        Common.debugLogger(TAG, "onTaskRemoved");
        callDozeService();
    }

    public void callDozeService() {
        if (!isMyServiceRunning(CoravitaService.class, this)) {
            Intent intent1 = new Intent(this, CoravitaService.class);
            RestartJobIntentService.enqueueWork(this, intent1);
            debugLogger(TAG, "Alarm started on doze mode");
        }
    }

    /* ********************************************************************************************** */

}


