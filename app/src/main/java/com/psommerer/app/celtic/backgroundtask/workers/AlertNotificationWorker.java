package com.psommerer.app.celtic.backgroundtask.workers;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.Vibrator;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.app.NotificationCompat;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.psommerer.app.celtic.R;
import com.psommerer.app.celtic.persistence.AppDatabaseClient;
import com.psommerer.app.celtic.persistence.SettingsData.AppSettingsObject;
import com.psommerer.app.celtic.utils.Common;
import com.psommerer.app.celtic.utils.SharedPrefUtils;
import com.psommerer.app.celtic.views.coremodules.MainActivity;

import java.text.MessageFormat;

import static com.psommerer.app.celtic.utils.Common.KEY_NOTIFICATION_TAG;
import static com.psommerer.app.celtic.utils.Common.debugLogger;


public class AlertNotificationWorker extends Worker {

    private static final String TAG = AlertNotificationWorker.class.getSimpleName();
    public static Context context;
    public static String KEY_RISK_VALUE = "risk_value";
    public static String KEY_RISK_TYPE = "risk_type"; //1=hourly 2=daily
    private WindowManager windowManager;

    private View floatyView;

    public AlertNotificationWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
        AlertNotificationWorker.context = context;
    }

    public static void callNotification(int riskValue, int riskType) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        String NOTIFICATION_CHANNEL_ID = "cora_channel_id_2020";
        String title = context.getString(R.string.app_name);
        String description = getNotificationInfo(riskValue, riskType, context);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, title, NotificationManager.IMPORTANCE_HIGH);
            notificationChannel.setDescription(description);
            notificationChannel.enableLights(true);
            notificationChannel.setVibrationPattern(new long[]{0, 1000, 500});
            notificationChannel.enableVibration(true);
            notificationManager.createNotificationChannel(notificationChannel);
        }

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context, NOTIFICATION_CHANNEL_ID);

        NotificationCompat.BigTextStyle textStyle = new NotificationCompat.BigTextStyle();
        textStyle.setBigContentTitle(title);
        textStyle.bigText(description);

        notificationBuilder
                .setWhen(System.currentTimeMillis())
                .setSmallIcon(R.drawable.ic_baseline_coronavirus_24)
                .setTicker(title)
                .setDefaults(NotificationCompat.DEFAULT_ALL)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setStyle(textStyle);

        /* Creates an explicit intent for an Activity in your app */
        Intent resultIntent = new Intent(context, MainActivity.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addParentStack(MainActivity.class);
        /* Adds the Intent that starts the Activity to the top of the stack */
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        notificationBuilder.setContentIntent(resultPendingIntent);
        //add actions
        Intent buttonIntent = new Intent(context, NotificationReceiver.class);
        int NOTIFICATION_ID = 1599;
        buttonIntent.putExtra(KEY_NOTIFICATION_TAG, NOTIFICATION_ID);
        PendingIntent dismissIntent = PendingIntent.getBroadcast(context, 0, buttonIntent, 0);
        notificationBuilder.addAction(R.drawable.ic_close, "CONFIRM", dismissIntent);

        notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(NOTIFICATION_ID, notificationBuilder.build());
    }

    static String getNotificationInfo(int value, int riskType, Context context) {
        String riskResult = Common.checkRiskScalesString(value, context);
        String message = context.getResources().getString(R.string.risknotificationtext1);
        if(riskType==2){//daily risk
            message = context.getResources().getString(R.string.risknotificationtext2);
        }
        return  message + (value) + "%\n" + context.getResources().getString(R.string.notification_riskstatus) + riskResult;
    }

    @Override
    public Result doWork() {
        try {

            int riskValue = getInputData().getInt(KEY_RISK_VALUE, 0);
            int riskType = getInputData().getInt(KEY_RISK_TYPE, 1);
            if (riskValue > 0) {
                AppSettingsObject appSettingsObject = AppDatabaseClient.getInstance(getApplicationContext()).getAppDatabase().appSettingsDao().getSettings();
                if (appSettingsObject.getNotificationType() == 1) { // show notification
                    callNotification(riskValue, riskType);
                } else { // show popup screen
                    callPopup(riskValue, riskType);
                }
            }
        } catch (Exception e) {
            Common.errorLogger(e, e.getMessage(), TAG);
            return Result.failure();
        }
        return Result.success();
    }

    private void callPopup(int value, int riskType) {
        Handler mHandler = new Handler(Looper.getMainLooper());
        mHandler.post(() -> {
            windowManager = (WindowManager) getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
            addOverlayView(value, riskType);
        });
    }

    private void addOverlayView(int value, int riskType) {

        final WindowManager.LayoutParams params;
        int layoutParamsType;

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            layoutParamsType = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;
        } else {
            layoutParamsType = WindowManager.LayoutParams.TYPE_PHONE;
        }

        params = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                layoutParamsType,
                0,
                PixelFormat.TRANSLUCENT);

        params.gravity = Gravity.CENTER | Gravity.START;
        params.x = 0;
        params.y = 0;

        FrameLayout interceptorLayout = new FrameLayout(getApplicationContext()) {
            @Override
            public boolean dispatchKeyEvent(KeyEvent event) {
                // Only fire on the ACTION_DOWN event, or you'll get two events (one for _DOWN, one for _UP)
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    // Check if the HOME button is pressed
                    if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
                        if (floatyView != null) {
                            windowManager.removeView(floatyView);
                            floatyView = null;
                        }
                        // As we've taken action, we'll return true to prevent other apps from consuming the event as well
                        return true;
                    }
                }
                // Otherwise don't intercept the event
                return super.dispatchKeyEvent(event);
            }
        };
        LayoutInflater inflater = ((LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE));
        if (inflater != null) {
            floatyView = inflater.inflate(R.layout.custom_alert_notification, interceptorLayout);
            windowManager.addView(floatyView, params);
            Button alertBtn = floatyView.findViewById(R.id.alert_dialog_close);
            alertBtn.setOnClickListener(v -> {
                if (floatyView != null) {
                    windowManager.removeView(floatyView);
                    floatyView = null;
                    SharedPrefUtils sharedPrefUtils = new SharedPrefUtils(getApplicationContext());
                    sharedPrefUtils.setNotificationStatus(false);
                }
            });

            String riskResult = Common.checkRiskScalesString(value, getApplicationContext());
            String message = getApplicationContext().getResources().getString(R.string.risknotificationtext1);//hourly
            if(riskType==2){//daily
                message = getApplicationContext().getResources().getString(R.string.risknotificationtext2);
            }
            AppCompatTextView textView1 = floatyView.findViewById(R.id.alerttext1);
            textView1.setText(MessageFormat.format("{0} {1}", message, value + "%"));

            AppCompatTextView textView2 = floatyView.findViewById(R.id.alerttext2);
            textView2.setText(String.format(Common.getAppLocale(), "%s %s", getApplicationContext().getResources().getString(R.string.notification_riskstatus), riskResult));

            addSoundAndVibrate();

        } else {
            debugLogger(TAG, "addOverlayView");
        }
    }

    private void addSoundAndVibrate() {
        final Vibrator vibe = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
        vibe.vibrate(500);
        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        MediaPlayer mp = MediaPlayer.create(context, uri);
        try {
            if (mp.isPlaying()) {
                mp.stop();
                mp.release();
            }
            mp.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
