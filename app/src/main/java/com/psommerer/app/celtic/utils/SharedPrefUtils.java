package com.psommerer.app.celtic.utils;

import android.content.Context;
import android.content.SharedPreferences;

import static android.content.Context.MODE_PRIVATE;
import static com.psommerer.app.celtic.utils.Common.KEY_CURRENTCITY;
import static com.psommerer.app.celtic.utils.Common.KEY_CURRENTCOUNTRY;
import static com.psommerer.app.celtic.utils.Common.KEY_CURRENTCOUNTRYCODE;
import static com.psommerer.app.celtic.utils.Common.KEY_CURRENT_COUNTERTAG;
import static com.psommerer.app.celtic.utils.Common.KEY_DARKMODE;
import static com.psommerer.app.celtic.utils.Common.KEY_DISCLAIMERSTATUS;
import static com.psommerer.app.celtic.utils.Common.KEY_HARDLOAD;
import static com.psommerer.app.celtic.utils.Common.KEY_LASTRISKVALUE;
import static com.psommerer.app.celtic.utils.Common.KEY_LATITUDE;
import static com.psommerer.app.celtic.utils.Common.KEY_LONGITUDE;
import static com.psommerer.app.celtic.utils.Common.KEY_NETWORKSTATUS;
import static com.psommerer.app.celtic.utils.Common.KEY_NOTIFICATIONSTATUS;
import static com.psommerer.app.celtic.utils.Common.KEY_SERVICESTATUS;


public class SharedPrefUtils {
    private static final String SHARED_PREF_NAME = "corashared";
    private static SharedPrefUtils INSTANCE = null;
    private final SharedPreferences preferences;

    public SharedPrefUtils(Context context) {
        preferences = context.getSharedPreferences(SHARED_PREF_NAME, MODE_PRIVATE);
    }

    public static SharedPrefUtils getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = new SharedPrefUtils(context);
            return INSTANCE;
        }
        return INSTANCE;
    }


    public void isDarkMode(Boolean b) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(KEY_DARKMODE, b);
        editor.apply();
    }

    public Boolean isDarkMode() {
        return preferences.getBoolean(KEY_DARKMODE, false);
    }

    public void clearAllPrefs() {
        preferences.edit().clear().apply();
    }

    public String getCurrentLatitude() {
        return preferences.getString(KEY_LATITUDE, "0");
    }

    public void setCurrentLatitude(double latitude) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(KEY_LATITUDE, String.valueOf(latitude));
        editor.apply();
    }


    public String getCurrentLongitude() {
        return preferences.getString(KEY_LONGITUDE, "0");
    }

    public void setCurrentLongitude(double latitude) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(KEY_LONGITUDE, String.valueOf(latitude));
        editor.apply();
    }

    public boolean getNetworkAvailability() {
        return preferences.getBoolean(KEY_NETWORKSTATUS, false);
    }

    public void setNetworkAvailability(boolean status) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(KEY_NETWORKSTATUS, status);
        editor.apply();
    }

    public boolean getDisclaimer() {
        return preferences.getBoolean(KEY_DISCLAIMERSTATUS, false);
    }

    public void setDisclaimer(boolean status) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(KEY_DISCLAIMERSTATUS, status);
        editor.apply();
    }

    public String getCurrentCountryCode() {
        return preferences.getString(KEY_CURRENTCOUNTRYCODE, "");
    }

    public void setCurrentCountryCode(String country) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(KEY_CURRENTCOUNTRYCODE, country);
        editor.apply();
    }

    public String getCurrentCountry() {
        return preferences.getString(KEY_CURRENTCOUNTRY, "");
    }

    public void setCurrentCountry(String country) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(KEY_CURRENTCOUNTRY, country);
        editor.apply();
    }

    public String getCurrentCity() {
        return preferences.getString(KEY_CURRENTCITY, "");
    }

    public void setCurrentCity(String city) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(KEY_CURRENTCITY, city);
        editor.apply();
    }

    public int getLastRiskValue() {
        return preferences.getInt(KEY_LASTRISKVALUE, 10);
    }

    public void setLastRiskValue(int riskValue) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(KEY_LASTRISKVALUE, riskValue);
        editor.apply();
    }

    public boolean getHardLoad() {
        return preferences.getBoolean(KEY_HARDLOAD, true);
    }

    public void setHardLoad(boolean scanMode) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(KEY_HARDLOAD, scanMode);
        editor.apply();
    }

    public boolean getNotificationStatus() {
        return preferences.getBoolean(KEY_NOTIFICATIONSTATUS, true);
    }

    public void setNotificationStatus(boolean scanMode) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(KEY_NOTIFICATIONSTATUS, scanMode);
        editor.apply();
    }

    public boolean getServiceStatus() {
        return preferences.getBoolean(KEY_SERVICESTATUS, false);
    }

    public void setServiceStatus(boolean scanMode) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(KEY_SERVICESTATUS, scanMode);
        editor.apply();
    }

    public int getCurrentCountTag() {
        return preferences.getInt(KEY_CURRENT_COUNTERTAG, 1);
    }

    public void setCurrentCountTag(int riskValue) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(KEY_CURRENT_COUNTERTAG, riskValue);
        editor.apply();
    }

}
