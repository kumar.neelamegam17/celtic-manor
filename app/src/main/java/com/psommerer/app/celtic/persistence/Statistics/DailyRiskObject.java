package com.psommerer.app.celtic.persistence.Statistics;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.psommerer.app.celtic.persistence.DateConverter;

import java.io.Serializable;
import java.util.Date;

@Entity
public class DailyRiskObject implements Serializable, Parcelable {
    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "isactive")
    private boolean isActive;

    @ColumnInfo(name = "created_at")
    @TypeConverters({DateConverter.class})
    private Date createdAt;

    private int riskValueInt;


    @TypeConverters({DateConverter.class})
    private Date createdTime;

    @TypeConverters({DateConverter.class})
    private Date createdDate;

    @ColumnInfo(name = "latitude")
    private String latitude;

    @ColumnInfo(name = "longitude")
    private String longitude;


    public DailyRiskObject(Parcel in) {
        id = in.readInt();
        isActive = in.readByte() != 0;
        riskValueInt = in.readInt();
        latitude = in.readString();
        longitude = in.readString();
    }

    public static final Creator<DailyRiskObject> CREATOR = new Creator<DailyRiskObject>() {
        @Override
        public DailyRiskObject createFromParcel(Parcel in) {
            return new DailyRiskObject(in);
        }

        @Override
        public DailyRiskObject[] newArray(int size) {
            return new DailyRiskObject[size];
        }
    };

    public DailyRiskObject() {

    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public int getRiskValueInt() {
        return riskValueInt;
    }

    public void setRiskValueInt(int riskValueInt) {
        this.riskValueInt = riskValueInt;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeByte((byte) (isActive ? 1 : 0));
        dest.writeInt(riskValueInt);
        dest.writeString(latitude);
        dest.writeString(longitude);
    }
}
