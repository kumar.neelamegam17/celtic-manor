package com.psommerer.app.celtic.persistence.MetaInfo;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface MetaInfoDao {

    @Query("SELECT * FROM MetaInfoObject")
    List<MetaInfoObject> getAll();

    @Insert
    void insert(MetaInfoObject task);

    @Delete
    void delete(MetaInfoObject task);

    @Update
    void update(MetaInfoObject task);

    @Query("SELECT * FROM MetaInfoObject WHERE Id=(SELECT MAX(Id)  FROM MetaInfoObject WHERE isactive=1)")
    MetaInfoObject getLastMetaInfoDao();

    @Query("DELETE FROM MetaInfoObject where created_at<:date")
    void deleteAll(long date);

}
