package com.psommerer.app.celtic.models;

import java.io.Serializable;
import java.util.Objects;

public class ScanBluetooth implements Serializable {
    String macAddress;

    public ScanBluetooth(String bluetoothDevice) {
        this.macAddress = bluetoothDevice;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ScanBluetooth that = (ScanBluetooth) o;
        return macAddress.equals(that.macAddress);
    }

    @Override
    public int hashCode() {
        return Objects.hash(macAddress);
    }
}
