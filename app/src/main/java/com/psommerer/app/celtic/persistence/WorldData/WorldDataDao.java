package com.psommerer.app.celtic.persistence.WorldData;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface WorldDataDao {
    @Query("SELECT * FROM WorldData")
    List<WorldData> getAll();

    @Insert
    void insert(WorldData task);

    @Delete
    void delete(WorldData task);

    @Update
    void update(WorldData task);

    @Query("SELECT * FROM WorldData WHERE Id = (SELECT MAX(Id)  FROM WorldData where isactive=1)")
    WorldData getLastWorldData();

    @Query("DELETE FROM WorldData where created_at<:date")
    void deleteAll(long date);

    @Query("UPDATE WorldData SET calculatedPrevalence=:prevalenceResult WHERE isactive=1")
    void updatePrevalenceResult(double prevalenceResult);

    @Query("UPDATE WorldData SET isactive=0 where Id = (SELECT MAX(Id)  FROM WorldData)")
    void updateIsActive();

}

