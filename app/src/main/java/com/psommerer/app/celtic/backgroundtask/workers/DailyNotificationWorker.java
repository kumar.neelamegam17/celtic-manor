package com.psommerer.app.celtic.backgroundtask.workers;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.work.Data;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.psommerer.app.celtic.R;
import com.psommerer.app.celtic.persistence.AppDatabaseClient;
import com.psommerer.app.celtic.persistence.SettingsData.AppSettingsObject;
import com.psommerer.app.celtic.utils.Common;

import static com.psommerer.app.celtic.backgroundtask.workers.AlertNotificationWorker.KEY_RISK_TYPE;
import static com.psommerer.app.celtic.utils.Common.DAILYNOTIFICATION_FLAG;
import static com.psommerer.app.celtic.utils.Common.convertRiskToInt;
import static com.psommerer.app.celtic.utils.Common.getRiskValueForLast24Hours;

public class DailyNotificationWorker extends Worker {
    WorkManager workManager;

    public DailyNotificationWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
    }

    @NonNull
    @Override
    public Result doWork() {
        workManager = WorkManager.getInstance(getApplicationContext());
        AppSettingsObject appSettingsObject = AppDatabaseClient.getInstance(getApplicationContext()).getAppDatabase().appSettingsDao().getSettings();
        if (appSettingsObject != null) {
            if (appSettingsObject.isNotificationEnabled() && appSettingsObject.getNotificationInterval().equals(getApplicationContext().getString(R.string.daily_risk))) {
                int calculated1DayRisk = convertRiskToInt(getRiskValueForLast24Hours(getApplicationContext()));
                if (appSettingsObject.getNotificationLevel().equalsIgnoreCase(Common.checkRiskScalesString(calculated1DayRisk, getApplicationContext()))) {
                    Data myData = new Data.Builder()
                            .putInt(AlertNotificationWorker.KEY_RISK_VALUE, calculated1DayRisk)
                            .putInt(KEY_RISK_TYPE, 2)
                            .build();
                    OneTimeWorkRequest dailyWork = new OneTimeWorkRequest.Builder(AlertNotificationWorker.class)
                            .addTag(Common.WORKER_TAG_DAILYNOTIFICATION)
                            .setInputData(myData)
                            .build();
                    workManager.enqueue(dailyWork);

                    DAILYNOTIFICATION_FLAG = true;
                }
            }
        }
        return Result.success();
    }
}
