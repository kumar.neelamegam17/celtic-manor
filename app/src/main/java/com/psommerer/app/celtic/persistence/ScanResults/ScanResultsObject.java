package com.psommerer.app.celtic.persistence.ScanResults;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.psommerer.app.celtic.persistence.DateConverter;

import java.io.Serializable;
import java.util.Date;

@Entity
public class ScanResultsObject implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "isactive")
    private boolean isActive;

    @ColumnInfo(name = "created_at")
    @TypeConverters({DateConverter.class})
    private Date createdAt;

    @ColumnInfo(name = "deviceCount")
    private int deviceCount;

    @ColumnInfo(name = "dataDump")
    private String dataDump;

    public String getDataDump() {
        return dataDump;
    }

    public void setDataDump(String dataDump) {
        this.dataDump = dataDump;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public int getDeviceCount() {
        return deviceCount;
    }

    public void setDeviceCount(int deviceCount) {
        this.deviceCount = deviceCount;
    }
}
