package com.psommerer.app.celtic.utils;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;

import androidx.work.Constraints;
import androidx.work.Data;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;

import com.psommerer.app.celtic.R;
import com.psommerer.app.celtic.backgroundtask.workers.AlertNotificationWorker;
import com.psommerer.app.celtic.persistence.AppDatabaseClient;
import com.psommerer.app.celtic.persistence.SettingsData.AppSettingsObject;

public class Experiment {

    public static void testHourlyNotification(Context contx) {

        final Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(() -> {
            AppSettingsObject appSettingsObject = AppDatabaseClient.getInstance(contx).getAppDatabase().appSettingsDao().getSettings();
            if (appSettingsObject != null) {
                if (appSettingsObject.isNotificationEnabled() && appSettingsObject.getNotificationInterval().equals(contx.getString(R.string.hourly_risk))) {
                    if (appSettingsObject.getNotificationLevel().equalsIgnoreCase(Common.checkRiskScalesString(50, contx))) {

                        final Data myData = new Data.Builder()
                                .putInt(AlertNotificationWorker.KEY_RISK_VALUE, 50)
                                .build();

                        Constraints constraints = new Constraints.Builder()
                                .setRequiresBatteryNotLow(true).build();

                        //hourly notification
                        OneTimeWorkRequest hourlyWork = new OneTimeWorkRequest.Builder(AlertNotificationWorker.class)
                                .addTag(Common.WORKER_TAG_HOURLYNOTIFICATION)
                                .setConstraints(constraints)
                                .setInputData(myData)
                                .build();
                        WorkManager.getInstance(contx).enqueue(hourlyWork);

                    }
                }
            }
        }, 10000);

    }

}
