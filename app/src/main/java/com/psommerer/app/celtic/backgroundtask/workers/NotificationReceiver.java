package com.psommerer.app.celtic.backgroundtask.workers;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.psommerer.app.celtic.utils.SharedPrefUtils;

import static com.psommerer.app.celtic.utils.Common.KEY_NOTIFICATION_TAG;

public class NotificationReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        int notificationId = intent.getIntExtra(KEY_NOTIFICATION_TAG, 0);
        NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        manager.cancel(notificationId);
        disableTheNotificationFor24Hours(context);
    }

    private void disableTheNotificationFor24Hours(Context context) {
        SharedPrefUtils sharedPrefUtils = new SharedPrefUtils(context);
        sharedPrefUtils.setNotificationStatus(false);
    }
}
