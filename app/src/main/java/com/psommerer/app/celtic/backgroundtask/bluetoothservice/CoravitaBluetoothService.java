package com.psommerer.app.celtic.backgroundtask.bluetoothservice;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.os.ParcelUuid;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;

import com.psommerer.app.celtic.R;

import java.util.ArrayList;
import java.util.List;

import no.nordicsemi.android.support.v18.scanner.BluetoothLeScannerCompat;
import no.nordicsemi.android.support.v18.scanner.ScanFilter;
import no.nordicsemi.android.support.v18.scanner.ScanSettings;

import static androidx.core.app.NotificationCompat.PRIORITY_MIN;
import static com.psommerer.app.celtic.utils.Common.ALARM_SERVICE_EXECUTION_INTERVAL_IN_MS_4;
import static com.psommerer.app.celtic.utils.Common.debugLogger;

public class CoravitaBluetoothService extends Service {
    private static final String TAG = CoravitaBluetoothService.class.getSimpleName();
    private static final int PENDING_INTENT_CODE = 123123;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private String createNotificationChannel(NotificationManager notificationManager){
        String channelId = "my_coravita_channelid";
        String channelName = "Cora Active";
        NotificationChannel channel = new NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_HIGH);
        channel.setImportance(NotificationManager.IMPORTANCE_NONE);
        channel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
        notificationManager.createNotificationChannel(channel);
        return channelId;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        BluetoothAdapter bAdapter = BluetoothAdapter.getDefaultAdapter();
        if (!bAdapter.isEnabled()) {
            bAdapter.enable();
        }

        ScanFilter.Builder builder = new ScanFilter.Builder();
        String serviceUuidString = "0000feaa-0000-1000-8000-00805f9b34fb";
        String serviceUuidMaskString = "FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF";
        ParcelUuid parcelUuid = ParcelUuid.fromString(serviceUuidString);
        ParcelUuid parcelUuidMask = ParcelUuid.fromString(serviceUuidMaskString);
        builder.setServiceUuid(parcelUuid, parcelUuidMask);
        ScanFilter filter = builder.build();


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            Intent intent1 = new Intent(getApplicationContext(), CoravitaBluetoothReceiver.class);
            intent1.setAction("com.psommerer.app.coravita.ACTION_FOUND");
            PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), PENDING_INTENT_CODE, intent1, PendingIntent.FLAG_UPDATE_CURRENT);
            BluetoothLeScannerCompat scanner = BluetoothLeScannerCompat.getScanner();
            ScanSettings settings = new ScanSettings.Builder()
                    .setScanMode(ScanSettings.SCAN_MODE_LOW_POWER)
                    .setUseHardwareBatchingIfSupported(true)
                    .setReportDelay(ALARM_SERVICE_EXECUTION_INTERVAL_IN_MS_4)
                    .build();
            List<ScanFilter> filters = new ArrayList<>();
            filters.add(new ScanFilter.Builder().build());
            filters.add(filter);
            scanner.startScan(filters, settings, getApplicationContext(), pendingIntent);
        } else {
            BluetoothLeScannerCompat scanner = BluetoothLeScannerCompat.getScanner();
            Intent intent1 = new Intent(getApplicationContext(), CoravitaBluetoothReceiver.class);
            intent1.setAction("com.psommerer.app.coravita.ACTION_FOUND");
            PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), PENDING_INTENT_CODE, intent1, PendingIntent.FLAG_UPDATE_CURRENT);
            ScanSettings settings = new ScanSettings.Builder()
                    .setUseHardwareBatchingIfSupported(false)
                    .setReportDelay(ALARM_SERVICE_EXECUTION_INTERVAL_IN_MS_4)
                    .setScanMode(ScanSettings.SCAN_MODE_LOW_POWER)
                    .build();
            List<ScanFilter> filters = new ArrayList<>();
            filters.add(new ScanFilter.Builder().build());
            scanner.startScan(filters, settings, getApplicationContext(), pendingIntent);
        }
        debugLogger(TAG, "callScan: started");

        return START_STICKY;
    }

    // Constants
    private static final int ID_SERVICE = 101;

    @Override
    public void onCreate() {
        super.onCreate();
        // do stuff like register for BroadcastReceiver, etc.

        // Create the Foreground Service
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        String channelId = Build.VERSION.SDK_INT >= Build.VERSION_CODES.O ? createNotificationChannel(notificationManager) : "";
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, channelId);
        Notification notification = notificationBuilder.setOngoing(true)
                .setContentTitle(getString(R.string.coravita_active))
                .setSmallIcon(R.mipmap.ic_launcher)
                .setPriority(PRIORITY_MIN)
                .setCategory(NotificationCompat.CATEGORY_SERVICE)
                .build();

        startForeground(ID_SERVICE, notification);
    }


    /* ********************************************************************************************** */

    @Override
    public void onDestroy() {
        super.onDestroy();
        debugLogger(TAG, "BLE service onDestroy");
    }
    /* ********************************************************************************************** */

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
        debugLogger(TAG, "BLE service onTaskRemoved");
    }


}
