package com.psommerer.app.celtic.views.submodules;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.Settings;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.core.app.NavUtils;
import androidx.lifecycle.LiveData;
import androidx.work.Constraints;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkInfo;
import androidx.work.WorkManager;

import com.psommerer.app.celtic.BuildConfig;
import com.psommerer.app.celtic.R;
import com.psommerer.app.celtic.backgroundtask.bluetoothservice.CoravitaBluetoothService;
import com.psommerer.app.celtic.backgroundtask.coravitaservices.CoravitaReceiver;
import com.psommerer.app.celtic.backgroundtask.workers.PrevalenceDataWorker;
import com.psommerer.app.celtic.databinding.ActivitySettingsBinding;
import com.psommerer.app.celtic.persistence.AppDatabaseClient;
import com.psommerer.app.celtic.persistence.SettingsData.AppSettingsObject;
import com.psommerer.app.celtic.persistence.WorldData.WorldData;
import com.psommerer.app.celtic.utils.Common;
import com.psommerer.app.celtic.utils.SharedPrefUtils;

import java.text.MessageFormat;

import static com.psommerer.app.celtic.utils.Common.convertPrevalenceData;
import static com.psommerer.app.celtic.utils.Common.debugLogger;
import static com.psommerer.app.celtic.utils.Common.errorLogger;
import static com.psommerer.app.celtic.utils.Common.getCurrentDateTime;

public class AppSettingsActivity extends AppCompatActivity {

    public static final String TAG = AppSettingsActivity.class.getSimpleName();
    private static final String KEY_CALCULATED_PREVALENCE = "calculated_prevalence";
    private static final String KEY_COUNTRY = "country";
    private static final String KEY_DATETIME = "datetime";
    private static final int MENU_ITEM_ITEM1 = 1;
    private static final int OVERLAY_PERMISSION_CODE = 5463; // can be anything
    ActivitySettingsBinding activitySettingsBinding;
    //**********************************************************************************************
    private final RadioGroup.OnCheckedChangeListener listener1 = new RadioGroup.OnCheckedChangeListener() {

        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            if (checkedId != -1) {
                activitySettingsBinding.radiogrp2.setOnCheckedChangeListener(null);
                activitySettingsBinding.radiogrp2.clearCheck();
                activitySettingsBinding.radiogrp2.setOnCheckedChangeListener(listener2);
            }
        }
    };
    private final RadioGroup.OnCheckedChangeListener listener2 = new RadioGroup.OnCheckedChangeListener() {

        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            if (checkedId != -1) {
                activitySettingsBinding.radiogrp1.setOnCheckedChangeListener(null);
                activitySettingsBinding.radiogrp1.clearCheck();
                activitySettingsBinding.radiogrp1.setOnCheckedChangeListener(listener1);
            }
        }
    };
    Dialog bottomDialog;
    private SharedPrefUtils sharedPrefUtils;

    //**********************************************************************************************
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            activitySettingsBinding = ActivitySettingsBinding.inflate(getLayoutInflater());
            View view = activitySettingsBinding.getRoot();
            setContentView(view);
            setUpActionBar();
            setInit();
        } catch (Exception e) {
            errorLogger(e, "onCreate: ", TAG);
        }
    }

    private void setInit() {

        try {
            sharedPrefUtils = new SharedPrefUtils(this);

            loadSettings();

            loadInstalledVersion();

            bottomDialog = new Dialog(this, R.style.BottomDialog);

            activitySettingsBinding.txtBluetoothscaninterval.setText(MessageFormat.format("{0} Minutes", Common.ALARM_SERVICE_EXECUTION_INTERVAL_IN_MS_5 / 60000));

            activitySettingsBinding.notificationnote.setText(MessageFormat.format("Note: Daily risk notification will happen at {0}", Common.getDailyNotificationTimeString()));

            activitySettingsBinding.notificationControl.setOnCheckedChangeListener((buttonView, isChecked) -> {
                if (isChecked) {
                    activitySettingsBinding.notificationSelection.setVisibility(View.VISIBLE);
                    activitySettingsBinding.notificationInterval.setVisibility(View.VISIBLE);
                } else {
                    activitySettingsBinding.notificationSelection.setVisibility(View.GONE);
                    activitySettingsBinding.notificationInterval.setVisibility(View.GONE);
                }
            });

            refreshContent();

            activitySettingsBinding.forcerefresh.setOnClickListener(v -> {
                if (sharedPrefUtils.getNetworkAvailability()) {
                    hardReload();
                } else {
                    Toast.makeText(this, getString(R.string.internet_not_available), Toast.LENGTH_SHORT).show();
                }
            });

            activitySettingsBinding.radiogrp1.setOnCheckedChangeListener(listener1);
            activitySettingsBinding.radiogrp2.setOnCheckedChangeListener(listener2);
        } catch (Exception e) {
            Common.errorLogger(e, e.getMessage(), TAG);
        }

    }

    void hardReload() {

        Constraints myConstraints = new Constraints.Builder().setRequiredNetworkType(NetworkType.CONNECTED).build();
        OneTimeWorkRequest runWorldDataRetrievalWorker = new OneTimeWorkRequest.Builder(PrevalenceDataWorker.class).setConstraints(myConstraints)
                .addTag(Common.WORKER_TAG_PREVALENCECALCULATION)
                .build();
        WorkManager.getInstance(getApplicationContext()).enqueue(runWorldDataRetrievalWorker);

        LiveData<WorkInfo> status = WorkManager.getInstance(getApplicationContext()).getWorkInfoByIdLiveData(runWorldDataRetrievalWorker.getId());
        status.observe(this, workStatus -> {
            switch (workStatus.getState()) {
                case FAILED:
                    debugLogger(TAG, "A Status is failed");
                    dismissBottomSheet();
                    Toast.makeText(getApplicationContext(), R.string.nosufficientdata, Toast.LENGTH_LONG).show();
                    break;
                case RUNNING:
                    debugLogger(TAG, "A Status is running");
                    showBottomSheet();
                    break;
                case SUCCEEDED:
                    debugLogger(TAG, "A Status is succeeded");
                    dismissBottomSheet();
                    refreshContent();
                    Toast.makeText(getApplicationContext(), R.string.updated, Toast.LENGTH_LONG).show();
                    break;
                default:
                    debugLogger(TAG, "A Status is unknown");
            }
        });


    }

    private void showBottomSheet() {
        View contentView = LayoutInflater.from(this).inflate(R.layout.bottom_sheet_layout, null);
        bottomDialog.setContentView(contentView);
        bottomDialog.setCancelable(false);
        ViewGroup.LayoutParams layoutParams = contentView.getLayoutParams();
        layoutParams.width = getResources().getDisplayMetrics().widthPixels;
        contentView.setLayoutParams(layoutParams);
        ImageView imageView = contentView.findViewById(R.id.loading_img);
        RotateAnimation rotate = new RotateAnimation(
                0, 360,
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f
        );
        rotate.setDuration(900);
        rotate.setRepeatCount(Animation.INFINITE);
        imageView.startAnimation(rotate);
        bottomDialog.getWindow().setGravity(Gravity.BOTTOM);
        bottomDialog.getWindow().setWindowAnimations(R.style.BottomDialog_Animation);
        if (!bottomDialog.isShowing()) {
            bottomDialog.show();
        }
    }

    private void dismissBottomSheet() {
        if (bottomDialog.isShowing()) {
            bottomDialog.dismiss();
        }
    }


    private void refreshContent() {
        Handler handler = new Handler(Looper.myLooper()) {
            @Override
            public void handleMessage(Message msg) {
                Bundle bundle = msg.getData();
                String prevalenceData = Common.checkStringEmpty(bundle.getString(KEY_CALCULATED_PREVALENCE));
                activitySettingsBinding.txtPrevalencecalculation.setText(MessageFormat.format("{0}", convertPrevalenceData(Double.parseDouble(prevalenceData))));
                activitySettingsBinding.txtPrevalencedatainfo.setText(Common.checkStringEmpty(String.format(Common.getAppLocale(), getString(R.string.prevalencedatainfo), bundle.getString(KEY_COUNTRY), bundle.getString(KEY_DATETIME))));
            }
        };

        Runnable runnable = () -> {
            Message msg = handler.obtainMessage();
            WorldData prevalenceResults = AppDatabaseClient.getInstance(getApplicationContext()).getAppDatabase().worldDataDao().getLastWorldData();
            if (prevalenceResults != null) {
                Bundle bundle = new Bundle();
                bundle.putString(KEY_CALCULATED_PREVALENCE, prevalenceResults.getCalculatedPrevalence());
                bundle.putString(KEY_COUNTRY, prevalenceResults.getCountryName());
                bundle.putString(KEY_DATETIME, Common.getDateString(prevalenceResults.getCreatedAt()));
                msg.setData(bundle);
                handler.sendMessage(msg);
            }
        };
        Thread riskThread = new Thread(runnable);
        riskThread.start();
    }

    private void setUpActionBar() {
        setUpTheme();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle(getResources().getString(R.string.appsettings_activity_title));
    }

    //**********************************************************************************************
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    //**********************************************************************************************
    public void setUpTheme() {
        SharedPrefUtils sharedPrefUtils = SharedPrefUtils.getInstance(this);
        if (sharedPrefUtils.isDarkMode()) {
            setTheme(R.style.AppTheme_Dark);
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getColor(R.color.colorthemeDarkPrimary)));
        } else {
            setTheme(R.style.AppTheme_Light);
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getColor(R.color.colorthemeLightPrimary)));
        }
    }

    //**********************************************************************************************
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        // If the permission has been checked
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == OVERLAY_PERMISSION_CODE) {
            if (Settings.canDrawOverlays(this)) {
                Toast.makeText(this, "accepted", Toast.LENGTH_SHORT).show();
                callInsert();
            } else {
                Toast.makeText(this, getString(R.string.enableoverlays), Toast.LENGTH_SHORT).show();
            }
        }
    }

    //**********************************************************************************************
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(Menu.FIRST, MENU_ITEM_ITEM1, Menu.NONE, getString(R.string.save)).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_WITH_TEXT);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case MENU_ITEM_ITEM1:
                saveSettings();
                return true;
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            default:
                return false;
        }
    }

    private void saveSettings() {

        if (getSelectedNotificationType() == 2) {
            //call
            if (Build.VERSION.SDK_INT >= 23) {
                if (!Settings.canDrawOverlays(getApplicationContext())) {
                    Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + getApplicationContext().getPackageName()));
                    startActivityForResult(intent, OVERLAY_PERMISSION_CODE);
                    Toast.makeText(getApplicationContext(), getString(R.string.popupnotification), Toast.LENGTH_LONG).show();
                } else {
                    callInsert();
                }
            }
        } else {
            callInsert();
        }
    }

    private void callInsert() {
        Handler handler = new Handler(Looper.myLooper()) {
            @Override
            public void handleMessage(Message msg) {
                Toast.makeText(AppSettingsActivity.this, getString(R.string.settingsupdated), Toast.LENGTH_LONG).show();
                //WorkManager.getInstance(getApplicationContext()).cancelAllWorkByTag(Common.WORKER_TAG_DAILYNOTIFICATION);
                //WorkManager.getInstance(getApplicationContext()).cancelAllWorkByTag(Common.WORKER_TAG_HOURLYNOTIFICATION);
            }
        };

        Runnable runnable = () -> {
            Message msg = handler.obtainMessage();
            String selectedRiskForNotification = "";
            int selectedRadioButtonId = activitySettingsBinding.radiogrp2.getCheckedRadioButtonId();
            RadioButton radioButton = findViewById(selectedRadioButtonId);
            boolean checkNext = false;
            if (radioButton != null) {
                selectedRiskForNotification = radioButton.getText().toString();
                checkNext = true;
            }

            if (!checkNext) {
                selectedRadioButtonId = activitySettingsBinding.radiogrp1.getCheckedRadioButtonId();
                radioButton = findViewById(selectedRadioButtonId);
                if (radioButton != null) {
                    selectedRiskForNotification = radioButton.getText().toString();
                }
            }

            int selectedDistance = getSelectedDistance();
            int selectedTime = getSelectedTime();
            int selectedNotificationType = getSelectedNotificationType();

            RadioButton notificationInterval = findViewById(activitySettingsBinding.radiogrp3.getCheckedRadioButtonId());

            AppSettingsObject appSettingsObject = new AppSettingsObject();
            appSettingsObject.setCreatedAt(getCurrentDateTime());
            appSettingsObject.setActive(true);
            appSettingsObject.setLanguage(activitySettingsBinding.language.getText().toString());
            appSettingsObject.setNotificationEnabled(activitySettingsBinding.notificationControl.isChecked());
            appSettingsObject.setNotificationLevel(selectedRiskForNotification);
            appSettingsObject.setNotificationInterval(notificationInterval.getText().toString());
            appSettingsObject.setContact_distance(selectedDistance);
            appSettingsObject.setCount_interval(selectedTime);
            appSettingsObject.setNotificationType(selectedNotificationType);
            AppDatabaseClient.getInstance(this).getAppDatabase().appSettingsDao().deleteAll();
            AppDatabaseClient.getInstance(this).getAppDatabase().appSettingsDao().insert(appSettingsObject);
            handler.sendMessage(msg);
        };
        Thread riskThread = new Thread(runnable);
        riskThread.start();
    }


    private int getSelectedTime() {
        int retValue = 15;
        int selectedRadioButtonId = activitySettingsBinding.timeRadiogrp3.getCheckedRadioButtonId();
        RadioButton radioButton = findViewById(selectedRadioButtonId);
        if (radioButton != null) {
            String selected = radioButton.getText().toString();
            if (selected.equals(getString(R.string._5_minutes))) {
                retValue = 5;
            } else if (selected.equals(getString(R.string._10_minutes))) {
                retValue = 10;
            }
        }
        return retValue;
    }

    private int getSelectedDistance() {
        int retValue = 2;
        int selectedRadioButtonId = activitySettingsBinding.distanceRadiogrp1.getCheckedRadioButtonId();
        RadioButton radioButton = findViewById(selectedRadioButtonId);
        if (radioButton != null) {
            String selected = radioButton.getText().toString();
            if (selected.equals(getString(R.string._4m))) {
                retValue = 4;
            } else if (selected.equals(getString(R.string._greater_4m))) {
                retValue = 5;
            }
        }
        return retValue;
    }

    private int getSelectedNotificationType() {
        int retValue = 1;
        int selectedRadioButtonId = activitySettingsBinding.notifytypes.getCheckedRadioButtonId();
        RadioButton radioButton = findViewById(selectedRadioButtonId);
        if (radioButton != null) {
            String selected = radioButton.getText().toString();
            if (selected.equals(getString(R.string.lable_pop_up))) {
                retValue = 2;
            }
        }
        return retValue;
    }

    private void loadSettings() {

        Handler handler = new Handler(Looper.myLooper()) {
            @Override
            public void handleMessage(Message msg) {
                AppSettingsObject appSettingsObject = (AppSettingsObject) msg.obj;
                if (appSettingsObject != null) {
                    boolean notificationStatus = appSettingsObject.isNotificationEnabled();
                    activitySettingsBinding.notificationControl.setChecked(notificationStatus);
                    if (notificationStatus) {
                        activitySettingsBinding.notificationSelection.setVisibility(View.VISIBLE);
                        activitySettingsBinding.notificationInterval.setVisibility(View.VISIBLE);
                    } else {
                        activitySettingsBinding.notificationSelection.setVisibility(View.GONE);
                        activitySettingsBinding.notificationInterval.setVisibility(View.GONE);
                    }
                    if (appSettingsObject.getNotificationLevel().equalsIgnoreCase(getString(R.string.veryhigh_risk))) {
                        activitySettingsBinding.radiogrp1.check(R.id.rb_veryhigh);
                    } else if (appSettingsObject.getNotificationLevel().equalsIgnoreCase(getString(R.string.high_risk))) {
                        activitySettingsBinding.radiogrp1.check(R.id.rb_highrisk);
                    } else if (appSettingsObject.getNotificationLevel().equalsIgnoreCase(getString(R.string.moderate_risk))) {
                        activitySettingsBinding.radiogrp2.check(R.id.rb_moderaterisk);
                    }

                    if (appSettingsObject.getNotificationInterval().equalsIgnoreCase(getString(R.string.hourly_risk))) {
                        activitySettingsBinding.radiogrp3.check(R.id.hourlyinterval);
                    } else {
                        activitySettingsBinding.radiogrp3.check(R.id.dailyinterval);
                    }
                    activitySettingsBinding.language.setText(appSettingsObject.getLanguage());

                    if (appSettingsObject.getCount_interval() == 5) {
                        activitySettingsBinding.timeRadiogrp3.check(R.id.time_5);
                    } else if (appSettingsObject.getCount_interval() == 10) {
                        activitySettingsBinding.timeRadiogrp3.check(R.id.time_10);
                    } else if (appSettingsObject.getCount_interval() == 15) {
                        activitySettingsBinding.timeRadiogrp3.check(R.id.time_15);
                    }

                    if (appSettingsObject.getContact_distance() == 2) {
                        activitySettingsBinding.distanceRadiogrp1.check(R.id.dt_rb_2m);
                    } else if (appSettingsObject.getContact_distance() == 4) {
                        activitySettingsBinding.distanceRadiogrp1.check(R.id.dt_rb_4m);
                    } else if (appSettingsObject.getContact_distance() == 5) {
                        activitySettingsBinding.distanceRadiogrp1.check(R.id.dt_rb_5m);
                    }

                    if (appSettingsObject.getNotificationType() == 1) {
                        activitySettingsBinding.notifytypes.check(R.id.notifytype1);
                    } else {
                        activitySettingsBinding.notifytypes.check(R.id.notifytype2);
                    }

                }
            }
        };

        Runnable runnable = () -> {
            Message msg = handler.obtainMessage();
            msg.obj = AppDatabaseClient.getInstance(this).getAppDatabase().appSettingsDao().getSettings();
            handler.sendMessage(msg);
        };
        Thread riskThread = new Thread(runnable);
        riskThread.start();


    }

    //**********************************************************************************************
    public void loadInstalledVersion() {
        String version = BuildConfig.VERSION_NAME;
        activitySettingsBinding.installedversion.setText(String.format(getString(R.string.installedversion), version));
    }

    //**********************************************************************************************
    public void showDisclaimer(View view) {

        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.disclaimer);
        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;


        AppCompatButton dButton = dialog.findViewById(R.id.dialog_close);
        dButton.setVisibility(View.GONE);


        WebView webView = dialog.findViewById(R.id.dataview);
        webView.loadData(getResources().getString(R.string.disclaimer), "text/html", "UTF-8");


        if (!dialog.isShowing()) {
            dialog.show();
            dialog.getWindow().setAttributes(lp);
        }
    }
    //**********************************************************************************************
    public void restartService(View view){
        CoravitaReceiver.setAlarm(this, Common.ALARM_SERVICE_EXECUTION_INTERVAL_IN_MS_5);
        debugLogger(TAG, "Cora Service restarted");
        Intent intent = new Intent(getApplicationContext(), CoravitaBluetoothService.class);
        debugLogger(TAG, "Cora BluetoothService: restarted");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForegroundService(intent);
        } else {
            startService(intent);
        }
        Toast.makeText(this, "Service restarted successfully...", Toast.LENGTH_SHORT).show();
    }
    //**********************************************************************************************

}
