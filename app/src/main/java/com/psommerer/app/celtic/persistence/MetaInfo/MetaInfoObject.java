package com.psommerer.app.celtic.persistence.MetaInfo;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.psommerer.app.celtic.persistence.DateConverter;

import java.io.Serializable;
import java.util.Date;

@Entity
public class MetaInfoObject implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "isactive")
    private boolean isActive;

    @ColumnInfo(name = "created_at")
    @TypeConverters({DateConverter.class})
    private Date createdAt;

    private Double infectionRate;
    private Double testingRate;
    private Double positivityRate;
    private Double fatalityRate;
    private Double prevalenceRate;
    private Double anonymityRate;
    private Double vaccinationRate;

    public Double getVaccinationRate() {
        return vaccinationRate;
    }

    public void setVaccinationRate(Double vaccinationRate) {
        this.vaccinationRate = vaccinationRate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Double getInfectionRate() {
        return infectionRate;
    }

    public void setInfectionRate(Double infectionRate) {
        this.infectionRate = infectionRate;
    }

    public Double getTestingRate() {
        return testingRate;
    }

    public void setTestingRate(Double testingRate) {
        this.testingRate = testingRate;
    }

    public Double getPositivityRate() {
        return positivityRate;
    }

    public void setPositivityRate(Double positivityRate) {
        this.positivityRate = positivityRate;
    }

    public Double getFatalityRate() {
        return fatalityRate;
    }

    public void setFatalityRate(Double fatalityRate) {
        this.fatalityRate = fatalityRate;
    }

    public Double getPrevalenceRate() {
        return prevalenceRate;
    }

    public void setPrevalenceRate(Double prevalenceRate) {
        this.prevalenceRate = prevalenceRate;
    }

    public Double getAnonymityRate() {
        return anonymityRate;
    }

    public void setAnonymityRate(Double anonymityRate) {
        this.anonymityRate = anonymityRate;
    }
}
