package com.psommerer.app.celtic.backgroundtask;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.psommerer.app.celtic.backgroundtask.coravitaservices.CoravitaReceiver;
import com.psommerer.app.celtic.backgroundtask.coravitaservices.CoravitaService;
import com.psommerer.app.celtic.utils.Common;

import static com.psommerer.app.celtic.utils.Common.isMyServiceRunning;

public class CoravitaBroadCastReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (!isMyServiceRunning(CoravitaService.class, context)) {
            if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
                CoravitaReceiver.setAlarm(context, Common.ALARM_SERVICE_EXECUTION_INTERVAL_IN_MS_5);
            }
        }
    }

}
